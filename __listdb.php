<?php
    header('Access-Control-Allow-Headers: X-Requested-With, origin, content-type');
    header('Access-Control-Allow-Origin: *');
    
    require_once("db.php");
    require_once("functions.php");
    $rep = error_reporting();
    error_reporting(0);

    $DEFAULT_SERVER = "komikid";
    $files = scandir("__data");
    $mds = array();
    foreach ($files as $file) {
        $x = explode(".", $file);
        if ($x[count($x)-1]=="mga") {
            $fdb = substr($file, 0, strlen($file)-4);
            $dbdir = dirname(__file__);
            $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
            $dbfile = "{$dbdir}{$separator}__data{$separator}{$fdb}.mga";
            //$dbh = new PDO("sqlite:{$dbfile}");
            //$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $strcon = array();
            $strcon["DB_DSN"] = "sqlite:".$dbfile;
            $strcon["DB_USER"] = "";
            $strcon["DB_PASS"] = "";
            $dbh = new DB($strcon);

            $rfsi = filesize($dbfile);
            $true_size = $rfsi >= 0 ? $rfsi : 4*1024*1024*1024 + $rfsi;
            $fsiz = inKB($true_size); //human_filesize($rfsi);

            $server = "";
            $chcount = 0;
            $pgcount = 0;
            $mname = $fdb;
            $tags = "";
            $sts = "";
            $cats = "";
            $lcap = "";
            $lpag = "";
            $covid = 1;
            $desc = "";
            try {
                /*
                $server = current($dbh->query("select coalesce(FValue,'') from MANGA where FKey = 'SERVER'")->fetch());
                if ($server=="") $server = $DEFAULT_SERVER;
                $server = strtolower($server);
                $chcount = current($dbh->query("select coalesce(FValue,'0') ccid from MANGA where FKey='COUNT_CHAPTER'")->fetch());
                $chcount = is_null($chcount) ? "0" : (($chcount=="") ? "0" : $chcount);
                $pgcount = current($dbh->query("select coalesce(FValue,'0') ccid from MANGA where FKey='COUNT_PAGE'")->fetch());
                $pgcount = is_null($pgcount) ? "0" : (($pgcount=="") ? "0" : $pgcount);
                $mname = current($dbh->query("select coalesce(FValue,'') ccid from MANGA where FKey='MANGA_NAME'")->fetch());
                $mname = is_null($mname) ? $fdb : (($mname=="") ? $fdb : $mname);
                $tags = current($dbh->query("select coalesce(FValue,'') ccid from MANGA where FKey='TAG'")->fetch());
                $tags = is_null($tags) ? "" : (($tags=="") ? "" : $tags);
                $sts = current($dbh->query("select coalesce(FValue,'') ccid from MANGA where FKey='STATUS'")->fetch());
                $sts = is_null($sts) ? "" : (($sts=="") ? "" : $sts);
                */
                $infos = $dbh->run("select fkey,fvalue from MANGA")->fetchAll();
                foreach($infos as $info){
                    if ($info["FKEY"] == "SERVER") $server = strtolower($info["FVALUE"]);
                    if ($info["FKEY"] == "COUNT_CHAPTER") $chcount = $info["FVALUE"];
                    if ($info["FKEY"] == "COUNT_PAGE") $pgcount = $info["FVALUE"];
                    if ($info["FKEY"] == "MANGA_NAME") $mname = (($info["FVALUE"]=="") ? $fdb : $info["FVALUE"]);
                    if ($info["FKEY"] == "TAG") $tags = $info["FVALUE"];
                    if ($info["FKEY"] == "STATUS") $sts = $info["FVALUE"];
                    if ($info["FKEY"] == "CATEGORIES") $cats = $info["FVALUE"];
                    if ($info["FKEY"] == "LAST_CHAPTER") $lcap = $info["FVALUE"];
                    if ($info["FKEY"] == "LAST_PAGE") $lpag = $info["FVALUE"];
                    if ($info["FKEY"] == "COVER_ID") $covid = $info["FVALUE"];
                    //if ($info["FKEY"] == "DESCRIPTION") $desc = $info["FVALUE"];
                }
                if ($lcap != "") $lcap = $dbh->run("select chapter from chapters where chapterid=?",array(0+$lcap))->fetchColumn();
                if ($lpag != "") $lpag = $dbh->run("select pagenum from pages where pageid=?",array(0+$lpag))->fetchColumn();
            } catch(Exception $e) {
                //
            }

            $mds[] = (object)array(
                'nama' => $fdb,
                'size' => $fsiz,
                'server' => $server,
                'chapters' => $chcount,
                'pages' => $pgcount,
                'manga' => trim($mname),
                'tag' => $tags,
                'status' => $sts,
                'categories' => $cats,
                'last_chapter' => $lcap,
                'last_page' => $lpag,
                'cover_id' => $covid,
                //'description' => $desc,
            );
        }
    }

    function comp($a, $b){
        return strcmp(strtoupper($a->manga), strtoupper($b->manga));
    }
    usort($mds, "comp");
    
    header('Content-Type: application/json');
    echo(json_encode($mds));

error_reporting($rep);
?>
