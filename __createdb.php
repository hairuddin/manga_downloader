<?php
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    $ndb = $_POST['new'];
    $nsv = strtoupper($_POST['server']);
    $loc = $_POST['loc'];
    $fname = "{$dbdir}{$separator}__data{$separator}{$ndb}.mga";
    $REGNAME = (isset($CLP_Company)) ? $CLP_Company : "";
    
    if (!file_exists($fname)) {
        copy($dbdir."{$separator}manga.db3", $fname);
        $dbh = new PDO("sqlite:{$fname}");
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("insert into MANGA (FKey, FValue) values (?,?)");
        //$dbh->query("insert into MANGA (FKey, FValue) values ('SERVER','{$nsv}')");
        //$dbh->query("insert into MANGA (FKey, FValue) values ('LOCATION','{$loc}')");
        $stm->execute(array("SERVER",$nsv));
        $stm->execute(array("LOCATION",$loc));
        $stm->execute(array("MANGA_NAME",""));
        $stm->execute(array("CATEGORIES",""));
        $stm->execute(array("STATUS",""));
        $stm->execute(array("TAG",""));
        $stm->execute(array("DESCRIPTION",""));
        $stm->execute(array("CREATOR",$REGNAME));
        echo "New database .: {$ndb} :. created.";
    } else {
        echo "Database  .: {$ndb} :. already exists.";
    }
?>
