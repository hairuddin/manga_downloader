<?php
    ini_set('max_execution_time', 0);
    require_once("cfg.php");
    require_once("functions.php");
    $DEFAULT_SERVER = "komikid";

    $dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";
    $dmname = isset($_GET['manga']) ? $_GET['manga'] : "^_^";

    if ($dmname != "^_^") {
        $mdname = substr($dmname,7,strlen($dmname)-11);
        $newloc = encodeURI($mdname);
        $dbname = $mdname;
    }

    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";

    $dbfile = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";

    $dbh = new PDO("sqlite:{$dbfile}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $server = strtolower(current($dbh->query("select coalesce(FValue,'') from MANGA where FKey = 'SERVER'")->fetch()));
    $manga = current($dbh->query("select coalesce(FValue,'') from MANGA where FKey = 'MANGA_NAME'")->fetch());
    if ($manga=="") $manga = $dbname;

    $qry = "pragma table_info(pages)";
    $pti = $dbh->query($qry)->fetchAll(PDO::FETCH_ASSOC);
    $ada = false;
    
    //print_r($pti);
    foreach($pti as $i){
        if (strtoupper($i['name'])=="IMGSIZE") $ada = true;
    }
    if (!$ada) {
        $dbh->query("alter table pages add column IMGSIZE bigint");
        $dbh->query("update pages set IMGSIZE=length(hex(img))/2");
    }
    
    $REGTO = (isset($CLP_Company)) ? "<span class='badge2' title='License'><i class='fas fa-lg fa-user'></i> Registered to {$CLP_Company}</span>" : "";
?>
<html>
    <head>
        <title><?php echo $manga; ?> @ Reader</title>
        <link rel="stylesheet" type="text/css" href="default.css">
        <script defer src="fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js"></script>
        <script type="text/javascript" src="jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="jquery.floatThead.min.js"></script>
        <script type="text/javascript" src="default.js"></script>
        <script type="text/javascript">
            var DB = "<?php echo $_GET['db']; ?>";
            var DOWN = false;
            var RETRY = 0;
            var SKIPCID = 0;
            var JXPAGE_UPDATING = false;
            var JXLOAD_UPDATING = false;
            var JXCHAPS_UPDATING = false;
            var oxp = false;
            var oxl = false;
            var oxc = false;
            var LAST_CLASS="";
            
            function jam(){var d=new Date();return d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();}

            function dlgOpen(cpt, src) {
                var bwe = window.innerWidth;
                var bhe = window.innerHeight;
                var d = $$('dlg');
                var id= $$('idlg');
                id.src = "about:blank";
                dw = 0.96*bwe;
                dh = 0.92*bhe;
                d.style.position = 'fixed';
                d.style.left = (bwe-dw)/2;
                d.style.top = 30;
                id.style.width = dw-10;
                id.style.height = dh-25;
                d.style.display = 'block';
                $$('cdlg').innerHTML = cpt;
                id.src = src;
                id.focus();
            }
            function dlgClose(){$$('idlg').src = 'about:blank'; $$('dlg').style.display = 'none';}
            function dlgCaption(cpt){ $$('cdlg').innerHTML = cpt; }

            function setStatus(txt) { $("#sts").html(txt); }
            function clrStatus() { $("#sts").html("Idle."); }

            function skipChapter(cid) {
                if (confirm('Skip this chapter?')) {
                    $.ajax({
                        url: '__skipchapter.php',
                        data: {'db': DB, 'id': cid},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            $("#pg"+SKIPCID).html("1");
                            $("#don"+SKIPCID).html("1");
                            SKIPCID = 0;
                            startTimer();
                        }
                    });
                    setStatus("<span class='fas fa-lg fa-spin fa-spinner'></span> Skipping chapter...");
                    SKIPCID = cid;
                }
            }

            function resetSkipped() {
                if (confirm('Reset all skipped pages?')) {
                    $.ajax({
                        url: '__skipchapter.php',
                        data: {'db': DB, 'reset': 'yes'},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            $('#debug').html(responseText);
                            loadChapList();
                        }
                    });
                    setStatus("<span class='fas fa-spin fa-lg fa-spinner'></span> Reset skipped pages...");
                }
            }

            function loadChapList() {
                if (!JXLOAD_UPDATING) {
                    JXLOAD_UPDATING = true;
                    setStatus("Reading db...");
                    $.ajax({
                        url: '__chapters.php',
                        data: {'db': DB},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            $("#chp").html(responseText);
                            clrStatus();
                            JXLOAD_UPDATING = false;
                            $("#tbl1").floatThead({
                                top: $("#atasan").innerHeight(),
                                zIndex: 45
                            });
                        }
                    });
                }
            }

            function loadChapter() {
                if (!JXCHAPS_UPDATING) {
                    JXCHAPS_UPDATING = true;
                    setStatus("<span class='fas fa-spin fa-lg fa-spinner'></span> Downloading chapter list (@"+jam()+")...");
                    $.ajax({
                        url: '<?="__servers/".$server?>/__readchapters.php',
                        data: {'db': DB},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            $('#debug').html(responseText);
                            clrStatus();
                            loadChapList();
                            JXCHAPS_UPDATING = false;
                        },
                        timeout: 10*60*1000 // 10 menit
                    });
                }
            }

            function loadPages() {
                if (!DOWN) {
                    DOWN = true;
                    setStatus("<span class='fas fa-spin fa-lg fa-spinner'></span> Downloading manga pages@"+jam()+"...");
                    $('#btnDownload').html('<i class="far fa-3x fa-stop-circle gap" style="color:maroon"></i><br>Stop');
                    RETRY = 0;
                    startTimer();
                } else {
                    DOWN = false;
                    setStatus("<span class='fas fa-spin fa-lg fa-spinner'></span> Downloading manga pages... (stopping@"+jam()+")");
                    $('#btnDownload').hide();
                }
            }

            function startTimer() {
                if (DOWN) {
                    setTimeout(function(){doLoadPages();}, 50);
                } else {
                    clrStatus();
                    $('#btnDownload').html('<i class="fas fa-3x fa-download gap"></i><br>Download');
                    $('#btnDownload').show();
                }
            }

            function doLoadPages() {
                if (!JXPAGE_UPDATING) {
                    JXPAGE_UPDATING = true;
                    setStatus("<span class='fas fa-spin fa-lg fa-spinner'></span> Downloading manga pages (#"+(1+RETRY)+" @ "+jam()+")...");
                    $.ajax({
                        url: '<?="__servers/".$server?>/__readpageauto.php',
                        data: {'db': DB, 'retry': RETRY},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            var data;
                            $('#debug').html("");
                            try {
                                eval('data=' + responseText);
                            } catch(e) {
                                $('#debug').html(responseText);
                                RETRY = 1+RETRY;
                                JXPAGE_UPDATING = false;
                                startTimer();
                                return ;
                            }
                            if (data.error!==0) {
                                $('#debug').html(responseText);
                                RETRY = 1+RETRY;
                                if (data.error==3) DOWN=false;
                            } else {
                                $("#pg"+data.id).html(data.pages);
                                $("#don"+data.id).html(data.done);
                                RETRY = 0;
                            }
                            JXPAGE_UPDATING = false;
                            startTimer();
                        }
                    });
                }
            }

            function readLastChapter() {dlgOpen('Read','read.php?db='+encodeURIComponent(DB));}
            function readChapter(id,txt) {dlgOpen('Read','read.php?db='+encodeURIComponent(DB)+'&ch='+id+'&str='+txt);}
            function buka_info(){dlgOpen(' Info','tbl_browser.php?db=<?=$dbname?>'); return false;}

            function downloadChapter(id,txt,res) {
                if ((JXPAGE_UPDATING) || (JXLOAD_UPDATING) || (JXCHAPS_UPDATING)) {
                    if (!confirm("This might stop current ajax request. Continue?")) {
                        //if (jxPage.updating) jxPage.abort();
                        //if (jxLoad.updating) jxLoad.abort();
                        //if (jxChaps.updating) jxChaps.abort();
                        return;
                    }
                }
                window.location = 'download.php?db='+encodeURIComponent(DB)+'&ch='+id+'&str='+txt+'&resize='+res;
            }

            function openPages(ch) {dlgOpen('View Pages','pages.php?db='+encodeURIComponent(DB)+'&ch='+ch);}

            function checkJx() {
                if(oxp!=JXPAGE_UPDATING){if(JXPAGE_UPDATING){$$("jPage").className="running";}else{$$("jPage").className="stopping";}oxp=JXPAGE_UPDATING;}
                if(oxl!=JXLOAD_UPDATING){if(JXLOAD_UPDATING){$$("jLoad").className="running";}else{$$("jLoad").className="stopping";}oxl=JXLOAD_UPDATING;}
                if(oxc!=JXCHAPS_UPDATING){if(JXCHAPS_UPDATING){$$("jChaps").className="running";}else{$$("jChaps").className="stopping";}oxc=JXCHAPS_UPDATING;}
            }

            function newChapter(){
                var nama = prompt('New Chapter Name?','New Chapter');
                if (nama != null){
                    $.ajax({
                        url: '__db.php',
                        data: {'db': DB, 'cmd': 'append chapter', 'cname': nama},
                        type: "POST",
                        cache: false,
                        success: function(obj, responseStatus, responseXML) {
                            if (obj.error == 0) {
                                window.location.reload(true);
                            } else {
                                alert(obj.message);
                            }
                        }
                    });
                }
            }

            function vakum(){
                if (confirm("Vacuum database?")){
                    setStatus("<span class='fas fa-spin fa-lg fa-spinner'></span> Vacuum database @ "+jam()+"...");
                    $.ajax({
                        url: '__db.php',
                        data: {'db': DB, 'cmd': 'vacuum'},
                        type: "POST",
                        cache: false,
                        success: function(obj, responseStatus, responseXML) {
                            clrStatus();
                            alert(obj.message);
                        }
                    });
                }
            }

            function delme(){
                if (confirm("Delete manga?")){
                    $.ajax({
                        url: '__db.php',
                        data: {'db': DB, 'cmd': 'hapus'},
                        type: "POST",
                        cache: false,
                        success: function(obj, responseStatus, responseXML) {
                            alert(obj.message);
                            window.location.href = "index.php";
                        }
                    });
                }
            }

            $(document).ready(function(){
                document.body.style.paddingTop = $("#atasan").innerHeight();
                loadChapList();
            });

            var si = setInterval(function(){checkJx();}, 500);
        </script>
    </head>
    <body>
        <div id="atasan" style="background-color:white; padding: 4px 6px; position: fixed; left:0px; top:0px; right:0px;">
            <div>
                <div class="topatas">
                <span class="badge"><a href="index.php" title="Home"><i class="fas fa-lg fa-home"></i></a><a href="javascript:void(0)" title="Manga Info" onclick="buka_info()"><i class="fas fa-lg fa-info-circle"></i></a><a href="javascript:void(0)" title="Chapters" onclick="loadChapter()"><i class="fas fa-lg fa-tags"></i></a><a href="javascript:void(0)" title="Download" onclick="loadPages()"><i class="fas fa-lg fa-download"></i></a><a href="javascript:void(0)" title="Read" onclick="readLastChapter()"><i class="fas fa-lg fa-glasses"></i></a></span>
                <span class="badge"><a href="javascript:void(0)" title="Reset Skipped Pages" onclick="resetSkipped()"><i class="fas fa-lg fa-recycle"></i></a><a href="javascript:void(0)" title="Add New Chapter" onclick="newChapter()"><i class="far fa-lg fa-file-alt"></i></a><a href="javascript:void(0)" title="Fix DB" onclick="vakum()"><i class="fas fa-lg fa-ambulance"></i></a><a href="javascript:void(0)" title="Delete" onclick="delme()"><i class="fas fa-lg fa-times-circle" style="color:maroon"></i></a></span>
                <span class="badge2" title="Server Service"><i class="fas fa-lg fa-globe"></i>  <?php echo $_SERVER["HTTP_HOST"]; ?></span>
                <span class="badge2" title="Proxy"><i class="fas fa-lg fa-external-link-alt"></i>  <?php echo ($HTTP_PROXY=="") ? "No Proxy" : $HTTP_PROXY; ?></span>
                <span class="badge2" title="Ajax"><i class="fas fa-lg fa-file-alt"></i> <span id="jPage" class="stopping">jxPage</span> <i class="fas fa-lg fa-cog"></i> <span id="jLoad" class="stopping">jxLoad</span> <i class="fas fa-lg fa-tags"></i> <span id="jChaps" class="stopping">jxChaps</span></span>
                <?php echo $REGTO; ?>
                </div>
            </div>
            <div>
                <br><div><b><?php echo $manga; ?> @ <?php echo $server; ?></b> &middot; <span>Status :: <span id="sts">Idle.</span></span></div>
                <br><div>
                    <button class="big" onclick="window.location.href='index.php'"><i class="fas fa-3x fa-home gap"></i><br>Home</button>
                    <button class="big" onclick="buka_info()"><i class="fas fa-3x fa-info-circle gap"></i><br>Info</button>
                    <button class="big" onclick="loadChapter()"><i class="fas fa-3x fa-tags gap"></i><br>Chapters</button>
                    <button class="big" id="btnDownload" onclick="loadPages()"><i class="fas fa-3x fa-download gap"></i><br>Download</button>
                    <button class="big" onclick="readLastChapter()"><i class="fas fa-3x fa-glasses gap"></i><br>Read</button>
                </div>
                <br>&nbsp;
            </div>
        </div>
        <div id="chp"></div>
        <div id="debug"></div>
        <div id="dlg" style="display:none;">
            <div class="caption"><a href="javascript:dlgClose()" class="cbtn"><span class="fas fa-lg fa-times-circle"></span></a> <span id="cdlg">Dialog Box Caption</span></div>
            <iframe id="idlg" src="about:blank" style="border-style: none;"></iframe>
        </div>
    </body>
</html>
