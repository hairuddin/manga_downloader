<?php
    ini_set('max_execution_time', 300);
    require_once("functions.php");

    $rep = error_reporting();
    error_reporting(0);

    $DEFAULT_SERVER = "tenmanga";
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    $dbfile = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";

    $rfsi = filesize($dbfile);
    $fsiz = inKB($rfsi); //human_filesize($rfsi);

    $dbh = new PDO("sqlite:{$dbfile}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    try {
        $sfile = current($dbh->query("select coalesce(FValue,'') from MANGA where FKey = 'COUNT_FILE_SIZE'")->fetch());
        if ($sfile!=$rfsi) {
            $chcount = current($dbh->query("select coalesce(count(chapterid),0) ccid from chapters")->fetch());
            $pgcount = current($dbh->query("select coalesce(count(pageid),0) ccid from pages where done=1")->fetch());
            if ($sfile=="") {
                $dbh->query("insert into MANGA (FValue, FKey) values ('$rfsi','COUNT_FILE_SIZE')");
                $dbh->query("insert into MANGA (FValue, FKey) values ('$chcount','COUNT_CHAPTER')");
                $dbh->query("insert into MANGA (FValue, FKey) values ('$pgcount','COUNT_PAGE')");
            } else {
                $dbh->query("update MANGA set FValue='$rfsi' where FKey='COUNT_FILE_SIZE'");
                $dbh->query("update MANGA set FValue='$chcount' where FKey='COUNT_CHAPTER'");
                $dbh->query("update MANGA set FValue='$pgcount' where FKey='COUNT_PAGE'");
            }
        }
        $server = current($dbh->query("select coalesce(FValue,'') from MANGA where FKey = 'SERVER'")->fetch());
        if ($server=="")
            $server = $DEFAULT_SERVER;
        $server = strtolower($server);
        $chcount = current($dbh->query("select coalesce(FValue,'0') ccid from MANGA where FKey='COUNT_CHAPTER'")->fetch());
        $pgcount = current($dbh->query("select coalesce(FValue,'0') ccid from MANGA where FKey='COUNT_PAGE'")->fetch());
    } catch(Exception $e) {
        $server = "";
    }

    $hasil = array();
    $hasil[] = $server;
    $hasil[] = $fsiz;
    $hasil[] = $chcount;
    $hasil[] = $pgcount;

    //echo "[\"{$server}\",\"{$fsiz}\",\"{$chcount}\",\"{$pgcount}\"]";
    echo json_encode($hasil);

    error_reporting($rep);
?>
