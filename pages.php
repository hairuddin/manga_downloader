<?php
    ini_set('max_execution_time', 300);
    require("db.php");
    require("table.php");

    $DEFAULT_SERVER = "tenmanga";
    $dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";
    $chid = isset($_GET['ch']) ? $_GET['ch'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";

    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }

    //$dbh = new PDO("sqlite:{$fname}");
    //$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $strcon = array();
    $strcon["DB_DSN"] = "sqlite:".$fname;
    $strcon["DB_USER"] = "";
    $strcon["DB_PASS"] = "";
    $dbh = new DB($strcon);

    $server = current($dbh->query("select coalesce(FValue,'') from MANGA where FKey = 'SERVER'")->fetch());
    if ($server=="")
        $server = $DEFAULT_SERVER;
    $server = strtolower($server);
    
    $qry = "pragma table_info(pages)";
    $pti = $dbh->query($qry)->fetchAll(PDO::FETCH_ASSOC);
    $ada = false;
    foreach($pti as $i){
        if (strtoupper($i['name'])=="IMGSIZE") $ada = true;
    }
    if (!$ada) {
        $dbh->query("alter table pages add column IMGSIZE bigint");
    }

    $c = $dbh->run("select * from chapters where chapterid=?", array($chid))->fetch();
    $chinfo = $c["CHAPTER"];

    $t = new PTable("tbl1","data");
    $t->open("thead")
        ->fillTr("th","ID|Page|Link|Img Size|Done|Action")
        ->close("thead");

    //$dbh->query("update pages set IMGSIZE=length(hex(img))/2 where chapterid={$chid} and imgsize is null");
    
    $qry = "select pageid, pagenum, imglink, done, imgsize isi from pages where chapterid={$chid}";
    $pags = $dbh->query($qry);
    $js = "";

    foreach($pags as $pag) {
        $isi = $pag['isi'];
        $done = $pag['DONE'];
        $pid = $pag['PAGEID'];
        if (($isi == 0) && ($done==1)) {
            $isi = "<a href=\"javascript:reset({$pid});\">[reset]</a>";
        } else {
            if ($isi != 0) {
                $isi = number_format($isi)." <a href=\"javascript:reset({$pid});\">[r]</a>";
            }
        }
        $imgl =  "<a href='read.php?db={$dbname}&page={$pid}'>[v]</a> <a href='".$pag['IMGLINK']."'>".$pag['IMGLINK']."</a>";
        //$tbl .= "<tr><td>{$pid}</td><td>{$pag['PAGENUM']}</td><td>{$imgl}</td><td>{$isi}</td><td>{$done}</td></tr>\n";
        $t->fillTr(
            "td",
            array(
                $pid,
                $pag['PAGENUM'],
                $imgl,
                array("td"=>$isi, "class"=>"kanan"),
                $done,
                "<button id='upl_".$pid."'>upload</button>"
            )
        );
        $js .= "upclick({
                    element: document.getElementById('upl_{$pid}'),
                    action: 'file_upload.php?db={$dbname}&id={$pid}',
                    onstart: function(filename){ document.getElementById('upl_{$pid}').innerHTML='Uploading'; },
                    oncomplete: function(response_data){
                        document.getElementById('upl_{$pid}').innerHTML = 'upload';
                        if (response_data!='OK') {
                            alert(response_data);
                        } else {
                            window.location.reload();
                        }
                    },
                });
                ";
    }
    //$tbl .= "</table>";
    $tbl = $t->toString();
?>
<html>
    <head>
        <title>Web Manga Downloader</title>
        <link rel="stylesheet" type="text/css" href="default.css">
        <style type="text/css">
            .pad8 { padding: 8px 0px; }
        </style>
        <script type="text/javascript" src="jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="jquery.floatThead.min.js"></script>
        <script type="text/javascript" src="upclick.js"></script>
        <script type="text/javascript" src="default.js"></script>
        <script type="text/javascript">
            var DB = "<?php echo $_GET['db']; ?>";

            function reset(pid) {
                if (confirm('Reset page?')) {
                    $.ajax({
                        url: '__db.php',
                        data: {'db': DB, 'cmd': 'Reset One Page', 'pid': pid},
                        type: "POST", cache: false,
                        success: function(obj, responseStatus, responseXML) {
                            if (obj.error == 0) {
                                window.location.reload(true);
                            } else {
                                alert(obj.message);
                            }
                        }
                    });
                }
            }

            function page_append(){
                if (confirm('Append empty page?')){
                    $.ajax({
                        url: '__db.php',
                        data: {'db': DB, 'cmd': 'append page', 'cid': '<?php echo $chid; ?>'},
                        type: "POST",
                        cache: false,
                        success: function(obj, responseStatus, responseXML) {
                            if (obj.error == 0) {
                                window.location.reload(true);
                            } else {
                                alert(obj.message);
                            }
                        }
                    });
                }
            }
        </script>
    </head>
    <body>
        <!--
        <div>
            <a href="index.php">[Home]</a>
            <a href="manga.php?db=<?php echo urlencode($dbname); ?>">[<?php echo $dbname; ?>]</a>
            [<?php echo $_SERVER["HTTP_HOST"]; ?>]
        </div>
        <hr>
        -->
        <div>
            <div id="p8">
            <div class="pad8">
                <b><?php echo $_GET['db']; ?> @ <?=$server?></b> // 
                <?= $chinfo ?> // 
                <?=date("Y-m-d H:i:s")?>
            </div>
            <div class="pad8">
                <input type="button" value="Refresh" onclick="window.location.reload();">
                <input type="button" value="Append" onclick="page_append()">
            </div>
            </div>
            <br><div id="chp"><?=$tbl?></div>
        </div>
        <div id="debug"></div>
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        $("body").keydown(function(event){
            if (event.which == 27) {
                // esc  {close reader}
                window.parent.dlgClose();
                event.preventDefault();
            }
        });

        $('#tbl1').floatThead({
            //top: $("#p8").innerHeight()
        });
        
        <?php echo $js; ?>
            
   });
</script>