<?php
    header('Access-Control-Allow-Headers: X-Requested-With, origin, content-type');
    header('Access-Control-Allow-Origin: *');

    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        die("{$fname} not found in filesystem.\nDatabase file not found.");
    }
    
    require("db.php");

    $strcon = array();
    $strcon["DB_DSN"] = "sqlite:".$fname;
    $strcon["DB_USER"] = "";
    $strcon["DB_PASS"] = "";
    $dbh = new DB($strcon);
    $sql = "select 
                max(case when fkey='MANGA_NAME' then fvalue end) as MANGA_NAME,
                max(case when fkey='CATEGORIES' then fvalue end) as CATEGORIES,
                max(case when fkey='STATUS' then fvalue end) as STATUS,
                max(case when fkey='TAG' then fvalue end) as TAG,
                max(case when fkey='DESCRIPTION' then fvalue end) as DESCRIPTION,
                max(case when fkey='LAST_PAGE' then fvalue end) as LAST_PAGE,
                max(case when fkey='LAST_CHAPTER' then fvalue end) as LAST_CHAPTER,
                coalesce(max(case when fkey='COVER_ID' then fvalue end),1) as COVER_ID
            from
                manga";
    $bok = $dbh->run($sql)->fetch();
    $stm = $dbh->run("select CHAPTERID, CHAPTER, LINK, PAGES, coalesce(DONECOUNT,0) DONE from chapters order by ChapterId")->fetchAll();
    
    $bok["CHAPTERS"] = $stm;
    $bok["DB"] = $dbname;
    
    header('Content-Type: application/json');
    echo json_encode($bok);
?>