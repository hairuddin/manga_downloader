<?php
    $err = error_reporting();
    error_reporting(0);
    
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "../../__data/{$dbname}.mga";
    if (!file_exists($fname)) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    require_once "vars.php";
    require_once "../../functions.php";

    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stm = $dbh->prepare("insert into chapters (chapter, link) values (?,?)");
    $stn = $dbh->prepare("insert into pages (PAGENUM, CHAPTERID, LINK, DONE) values (?, ?, ?, 0)");
    $stp = $dbh->prepare("update chapters set pages=? where chapterid=?");

    $skip = "";
    $skip = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'CHAPTER_SKIP'")->fetch());
    $skipping = ($skip != "") ? true : false;

    try {
        $loc = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'LOCATION'")->fetch());
        if ($loc == "") $loc = $dbname;
        $loc = strtolower($loc);
        echo "{$BASE}manga/{$loc}\n";
        $page = get_data($BASE."manga/".$loc."");
    } catch (Exception $e) {
        die("Error loading page. ".$e);
    }
    
    $ccount = 0;
    $pcount = 0;
    $cid = 0;
    $cstr = "";
    
    $dom = new DOMDocument;
    $dom->loadHTML($page);

    $ul = $dom->getElementsByTagName('ul');
    foreach ($ul as $u1) {
        if ($u1->getAttribute('class')=="chapters") {
            $li = $u1->getElementsByTagName('li');
            $arrch = array();
            foreach ($li as $ii) {
                $cls = $ii->getAttribute('class');
                if (substr($cls,0,7) == "volume-") {
                    $anc1 = $ii->getElementsByTagName('a');
                    $anc = $anc1->item(0);
                    
                    $ch = $anc->nodeValue;
                    $lnk =$anc->getAttribute('href');
                    $arrch[] = "{$ch}|{$lnk}";
                } else {
                    //echo "gakada; ";
                }
                $cls = "";
            }
            $charr = array_reverse($arrch);
            foreach ($charr as $chs) {
                $arr = explode('|',$chs);
                if ($skipping) {
                    if (strtoupper(trim($skip)) == strtoupper(trim($arr[0])))
                        $skipping = false;
                    continue;
                }
                $stm->bindParam(1, $arr[0]);
                $stm->bindParam(2, $arr[1]);
                try {
                    $stm->execute();
                } catch (Exception $e) {
                    // do nothing
                }
            }
        }
    }
    
    $nams = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'MANGA_NAME'")->fetch());
    if ($nams=="") {
        $stm = $dbh->prepare("update MANGA set FValue=? where FKey=? and coalesce(FValue,'')=''");

        $h2s = $dom->getElementsByTagName('h2');
        foreach ($h2s as $h2) {
            if ($h2->getAttribute('class')=="widget-title") {
                $nams = $h2->nodeValue;
                $stm->execute(array($nams, 'MANGA_NAME'));
                break;
            }
        }
        
        $spans = $dom->getElementsByTagName('span');
        foreach($spans as $spn) {
            if ($spn->getAttribute('class')=="label label-success") {
                $nams = $spn->nodeValue;
                $stm->execute(array($nams, 'STATUS'));
                break;
            }
        }
        
        $aas = $dom->getElementsByTagName('a');
        $cat = "";
        foreach($aas as $aa) {
            $cls = $aa->getAttribute('href');
            $ac = explode('/',$cls);
            if (count($ac)>=3) {
                if ($ac[count($ac)-2]=="category") {
                    if (strlen($cat)>0) $cat.=", ";
                    $cat.=$aa->nodeValue;
                }
            }
        }
        $stm->execute(array($cat,'CATEGORIES'));
        
        $divs = $dom->getElementsByTagName('div');
        $sum = "";
        foreach($divs as $div) {
            if ($div->getAttribute('class')=='well') {
                $pes = $div->getElementsByTagName('p');
                $sum = $pes->item(0)->nodeValue;
            }
        }
        $stm->execute(array($sum,'DESCRIPTION'));
    }
    
    echo "Success.";
    error_reporting($err);
?>
