<?php
    $err = error_reporting();
    error_reporting(0);
    
    $dbname = isset($_POST['db']) ? $_POST['db'] : "";
    if ($dbname=="")$dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";

    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "../../__data/{$dbname}.mga";
    if (!file_exists($fname)) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    require_once "vars.php";
    require_once "../../functions.php";

    $stm = $dbh->prepare("insert into chapters (chapter, link) values (?,?)");
    $stn = $dbh->prepare("insert into pages (PAGENUM, CHAPTERID, LINK, DONE) values (?, ?, ?, 0)");
    $stp = $dbh->prepare("update chapters set pages=? where chapterid=?");
    
    $skip = "";
    $skip = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'CHAPTER_SKIP'")->fetch());
    $skipping = ($skip != "") ? true : false;

    try {
        $loc = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'LOCATION'")->fetch());
        if ($loc == "") $loc = $dbname;
        $lok = $BASE."manga/".$loc;
        $page = get_data($lok);
        //file_put_contents("tes.html",$page);
    } catch (Exception $e) {
        die("Error loading page. ".$e);
    }
    
    if ($page=="") {
        die("Error loading page.");
    }
    //echo("$page <br>");
    $ccount = 0;
    $pcount = 0;
    $cid = 0;
    $cstr = "";
    
    $dom = new DOMDocument;
    $dom->loadHTML($page);

    $uls = $dom->getElementsByTagName('h5');
    foreach($uls as $ul) {
        $ulcl = $ul->getAttribute("class");
        if ($ulcl == "chapter-title-rtl") {
            $aas = $ul->getElementsByTagName('a')->item(0);

            $a0 = trim(strip_tags_content($ul->getElementsByTagName('em')->item(0)->nodeValue));
            $a1 = $aas->getAttribute("href")."";
            $arro[] = $a0.'|'.$a1;
        }
    }

    $rev = array_reverse($arro);
    //print_r($rev);
    foreach ($rev as $itm) {
        $arr = explode('|', $itm);
        if ($skipping) {
            if (strtoupper(trim($skip)) == strtoupper(trim($arr[0])))
                $skipping = false;
            continue;
        }
        $stm->bindParam(1, $arr[0]);
        $stm->bindParam(2, $arr[1]);
        try {
            $stm->execute();
        } catch (Exception $e) {
            //
        }
    }
    
    $nams = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'MANGA_NAME'")->fetch());
    if ($nams=="") {
        $stm = $dbh->prepare("update MANGA set FValue=? where FKey=? and coalesce(FValue,'')=''");

        $nams = $dom->getElementsByTagName('h2')->item(0)->nodeValue;
        $stm->execute(array($nams, 'MANGA_NAME'));

        $divs = $dom->getElementsByTagName('div');
        foreach($divs as $div){
            $dcl = $div->getAttribute("class");
            if ($dcl == "well"){
                $nams = $div->getElementsByTagName('p')->item(0)->nodeValue;
                //$nams = $div->nodeValue;
                $stm->execute(array($nams, 'DESCRIPTION'));
            }
        }
        /*        
        $t1 = $dom->getElementsByTagName('table')->item(0);
        $t2 = $t1->getElementsByTagName('table')->item(0);
        $tds = $t2->getElementsByTagName('td');
        foreach($tds as $td) {
        }
        */
    }

    echo "Success.";
    error_reporting($err);
?>
