<?php
    $err = error_reporting();
    error_reporting(0);
    
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "../../__data/{$dbname}.mga";
    if (!file_exists($fname)) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    require_once "vars.php";
    require_once "../../functions.php";

    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stm = $dbh->prepare("insert into chapters (chapter, link) values (?,?)");
    $stn = $dbh->prepare("insert into pages (PAGENUM, CHAPTERID, LINK, DONE) values (?, ?, ?, 0)");
    $stp = $dbh->prepare("update chapters set pages=? where chapterid=?");

    $skip = "";
    $skip = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'CHAPTER_SKIP'")->fetch());
    $skipping = ($skip != "") ? true : false;

    try {
        $loc = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'LOCATION'")->fetch());
        if ($loc == "") $loc = $dbname;
        //echo "{$BASE}{$loc}\n";
        $page = get_data($BASE.$loc);
    } catch (Exception $e) {
        die("Error loading page. ".$e);
    }
    
    //file_put_contents("chap.html", $page);

    $ccount = 0;
    $pcount = 0;
    $cid = 0;
    $cstr = "";
    
    $dom = new DOMDocument;
    $dom->loadHTML($page);

    $ul = $dom->getElementsByTagName('div');
    foreach ($ul as $u1) {
        $tyle = $u1->getAttribute('style');
        if (substr($tyle,0,86)=="-moz-border-radius: 5px 5px 5px 5px; border: 1px solid rgb(255, 204, 0); color: black;" ||
            substr($tyle,0,54)=="-moz-border-radius: 5px 5px 5px 5px;  no-repeat scroll") {
            $li = $u1->getElementsByTagName('a');
            $arrch = array();
            foreach ($li as $ii) {
                $ch = $ii->nodeValue;
                $lnk =$ii->getAttribute('href');
                $arrch[] = "{$ch}|{$lnk}";
            }
            $charr = array_reverse($arrch);
            foreach ($charr as $chs) {
                $arr = explode('|',$chs);
                if ($skipping) {
                    if (strtoupper(trim($skip)) == strtoupper(trim($arr[0])))
                        $skipping = false;
                    continue;
                }
                $stm->bindParam(1, $arr[0]);
                $stm->bindParam(2, $arr[1]);
                try {
                    $stm->execute();
                } catch (Exception $e) {
                    // do nothing
                }
            }
        }
    }
    
    $nams = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'MANGA_NAME'")->fetch());
    if ($nams=="") {
        /*
        $stm = $dbh->prepare("update MANGA set FValue=? where FKey=? and coalesce(FValue,'')=''");

        $h2s = $dom->getElementsByTagName('h2');
        foreach ($h2s as $h2) {
            if ($h2->getAttribute('class')=="widget-title") {
                $nams = $h2->nodeValue;
                $stm->execute(array($nams, 'MANGA_NAME'));
                break;
            }
        }
        
        $spans = $dom->getElementsByTagName('span');
        foreach($spans as $spn) {
            if ($spn->getAttribute('class')=="label label-success") {
                $nams = $spn->nodeValue;
                $stm->execute(array($nams, 'STATUS'));
                break;
            }
        }
        
        $aas = $dom->getElementsByTagName('a');
        $cat = "";
        foreach($aas as $aa) {
            $cls = $aa->getAttribute('href');
            $ac = explode('/',$cls);
            if (count($ac)>=3) {
                if ($ac[count($ac)-2]=="category") {
                    if (strlen($cat)>0) $cat.=", ";
                    $cat.=$aa->nodeValue;
                }
            }
        }
        $stm->execute(array($cat,'CATEGORIES'));
        
        $divs = $dom->getElementsByTagName('div');
        $sum = "";
        foreach($divs as $div) {
            if ($div->getAttribute('class')=='well') {
                $pes = $div->getElementsByTagName('p');
                $sum = $pes->item(0)->nodeValue;
            }
        }
        $stm->execute(array($sum,'DESCRIPTION'));
        */
    }
    
    echo "Success.";
    error_reporting($err);
?>
