<?php
    $err = error_reporting();
    error_reporting(0);
    
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "../../__data/{$dbname}.mga";
    if (!file_exists($fname)) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    require_once "vars.php";
    require_once "../../functions.php";

    $stm = $dbh->prepare("insert into chapters (chapter, link) values (?,?)");
    $stn = $dbh->prepare("insert into pages (PAGENUM, CHAPTERID, LINK, DONE) values (?, ?, ?, 0)");
    $stp = $dbh->prepare("update chapters set pages=? where chapterid=?");

    $skip = "";
    $skip = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'CHAPTER_SKIP'")->fetch());
    $skipping = ($skip != "") ? true : false;

    try {
        $loc = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'LOCATION'")->fetch());
        if ($loc == "") $loc = $dbname;
        $page = get_data($BASE."comic/".$loc);
        //file_put_contents("tes.html",$page);
    } catch (Exception $e) {
        die("Error loading page; ".$BASE."comic/".$loc);
    }
    
    if ($page=="") {
        die("Error loading page. ".$BASE."comic/".$loc);
    }
    
    $ccount = 0;
    $pcount = 0;
    $cid = 0;
    $cstr = "";
    
    $dom = new DOMDocument;
    $dom->loadHTML($page);

    $uls = $dom->getElementsByTagName('ul');
    foreach($uls as $ul) {
        $ucl = $ul->getAttribute("class");
        if ($ucl == "basic-list") {
            $lis = $ul->getElementsByTagName('li');
            foreach ($lis as $li) {
                $a = $li->getElementsByTagName('a')[0];
                $arro[] = DOMinnerHTML($a).'|'.$a->getAttribute("href");
            }
        }
    }

    $rev = array_reverse($arro);
    //print_r($rev);
    foreach ($rev as $itm) {
        $arr = explode('|', $itm);
        if ($skipping) {
            if (strtoupper(trim($skip)) == strtoupper(trim($arr[0])))
                $skipping = false;
            continue;
        }
        $stm->bindParam(1, $arr[0]);
        $stm->bindParam(2, $arr[1]);
        try {
            $stm->execute();
        } catch (Exception $e) {
            //
        }
    }
    
    $nams = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'MANGA_NAME'")->fetch());
    if ($nams=="") {
        $stm = $dbh->prepare("update MANGA set FValue=? where FKey=? and coalesce(FValue,'')=''");

        $nams = $dom->getElementsByTagName('h1')->item(1)->nodeValue;
        $stm->execute(array(trim($nams), 'MANGA_NAME'));

        $divs = $dom->getElementsByTagName('p');
        foreach($divs as $div){
            $dcl = $div->getAttribute("class");
            if ($dcl == "pdesc"){
                $nams = $div->nodeValue;
                $stm->execute(array($nams, 'DESCRIPTION'));
            }
        }
        /*        
        $t1 = $dom->getElementsByTagName('table')->item(0);
        $t2 = $t1->getElementsByTagName('table')->item(0);
        $tds = $t2->getElementsByTagName('td');
        foreach($tds as $td) {
        }
        */
    }

    echo "Success.";
    error_reporting($err);
?>
