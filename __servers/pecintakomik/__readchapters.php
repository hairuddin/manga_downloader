<?php
    $err = error_reporting();
    error_reporting(0);
    
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "../../__data/{$dbname}.mga";
    if (!file_exists($fname)) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    require_once "vars.php";
    require_once "../../functions.php";

    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stm = $dbh->prepare("insert into chapters (chapter, link) values (?,?)");
    $stn = $dbh->prepare("insert into pages (PAGENUM, CHAPTERID, LINK, DONE) values (?, ?, ?, 0)");
    $stp = $dbh->prepare("update chapters set pages=? where chapterid=?");
    
    $skip = "";
    $skip = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'CHAPTER_SKIP'")->fetch());
    $skipping = ($skip != "") ? true : false;

    try {
        $loc = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'LOCATION'")->fetch());
        if ($loc == "") $loc = $dbname;
        //echo "{$BASE}{$loc}\n";
        $page = get_data($BASE.$loc."");
    } catch (Exception $e) {
        die("Error loading page. ".$e);
    }
    
    $ccount = 0;
    $pcount = 0;
    $cid = 0;
    $cstr = "";
    
    //file_put_contents("chp.html", $page);

    $dom = new DOMDocument;
    $dom->loadHTML($page);

    $dctr = 0;
    $divs = $dom->getElementsByTagName('div');
    foreach ($divs as $div) {
        if ($div->getAttribute('class')=="post-cnt") {
            if ($dctr>0) {
                $lis = $div->getElementsByTagName('li');
                foreach ($lis as $li) {
                    $a = $li->getElementsByTagName('a')->item(0);
                    $ch = $a->nodeValue;
                    $lnk = substr($BASE,0,-1).$a->getAttribute('href');
                    $arrch[] = "{$ch}|{$lnk}";
                }
                $charr = array_reverse($arrch);
                foreach ($charr as $chs) {
                    $arr = explode('|',$chs);
                    if ($skipping) {
                        if (strtoupper(trim($skip)) == strtoupper(trim($arr[0])))
                            $skipping = false;
                        continue;
                    }
                    $stm->bindParam(1, $arr[0]);
                    $stm->bindParam(2, $arr[1]);
                    try {
                        $stm->execute();
                    } catch (Exception $e) {
                        // do nothing
                    }
                }
            } else {
            }
            $dctr++;
        }
    }

    foreach ($divs as $div) {
        if ($div->getAttribute('class')=="post-cnt") {
            // GET INFO
            $nams = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'MANGA_NAME'")->fetch());
            if ($nams=="") {
                $stm = $dbh->prepare("update MANGA set FValue=? where FKey=? and coalesce(FValue,'')=''");
        
                $h2 = $div->getElementsByTagName('h2')->item(0);
                $nams = $h2->nodeValue;
                $stm->execute(array($nams, 'MANGA_NAME'));
                
                $lis = $div->getElementsByTagName('li');
                foreach($lis as $li) {
                    $str = $li->nodeValue;
                    if (substr(trim($str),0,6)=="Genre:") {
                        $nams = trim(substr($str,6));
                        $stm->execute(array($nams, 'CATEGORIES'));
                    }
                    if (substr(trim($str),0,9)=="Sinopsis:") {
                        $nams = trim(substr($str,9));
                        $stm->execute(array($nams, 'DESCRIPTION'));
                    }
                }
            }
            break;
        }
    }    
    echo "Success.";
    error_reporting($err);
?>
