<?php
    date_default_timezone_set('Asia/Jakarta');
    $err = error_reporting();
    error_reporting(E_ERROR);
    //error_reporting(0);
    
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $retry = isset($_POST['retry']) ? $_POST['retry'] : 0;
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";

    $hasil = array('id'=>0, 'pages'=>0, 'done'=>0, 'error'=>0, 'msg'=>'', 'retry'=>$retry);
    $fname = "../../__data/{$dbname}.mga";

    if (!file_exists($fname)) {
        $hasil['error'] = 1;
        $hasil['msg'] = "Database file not found.\n{$fname} not found in filesystem.";
        $hasil['date'] = date('Y-m-d H:i:s');
        error_reporting($err);
        die(json_encode($hasil));
    }
    
    require_once "vars.php";
    require_once "../../functions.php";
    
    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $loc = current($dbh->query("select min(coalesce(FValue,'')) from MANGA where FKey = 'LOCATION'")->fetch());
    if ($loc == "") $loc = $dbname;
    //$loc = strtolower($loc);

    $qry = "select * from chapters where coalesce(pages,0)>coalesce(donecount,0) or coalesce(pages,0)=0
            order by chapterid
            limit 1";
    $rst = $dbh->query($qry)->fetch(PDO::FETCH_ASSOC);
    
    if ($rst == false) {
        $hasil['error'] = 3;
        $hasil['msg'] = "No next page.";
        $hasil['date'] = date('Y-m-d H:i:s');
        error_reporting($err);
        die(json_encode($hasil));
    }
    
    $CHAPTERID = $rst['CHAPTERID'];
    $PAGES = isset($rst['PAGES']) ? (null!=$rst['PAGES'] ? $rst['PAGES'] : 0) : 0;
    $DONE = isset($rst['DONECOUNT']) ? (null!=$rst['DONECOUNT'] ? $rst['DONECOUNT'] : 0) : 0;
    $LINK = isset($rst['LINK']) ? (null!=$rst['LINK'] ? $rst['LINK'] : "") : "";
    
    if ($LINK=="") {
        $hasil['error'] = 5;
        $hasil['msg'] = "No link for chapter.";
        $hasil['date'] = date('Y-m-d H:i:s');
        error_reporting($err);
        die(json_encode($hasil));
    }
    
    $ar1 = explode("/",$LINK);
    $cloc = strtolower($ar1[count($ar1)-1]);
    
    $qry = "select PAGEID, PAGENUM, CHAPTERID, LINK, IMGLINK, DONE
        from pages
        where CHAPTERID={$CHAPTERID} and coalesce(DONE,0)=0
        order by pageid
        limit 1";
        
    $rst = $dbh->query($qry)->fetch(PDO::FETCH_ASSOC);

    $ambil = "";
    $page="";
    if ($rst==false) {
        // belum ada pages / new chapter
        if ($retry >= $RETRY) {
            // maximum retry; skip chapter;
            $dbh->query("update chapters set pages=1, donecount=1 where chapterid={$CHAPTERID}");
            $hasil['id'] = $CHAPTERID;
            $hasil['pages'] = 1;
            $hasil['done'] = 1;
            $hasil['err'] = 0;
            $hasil['msg'] = "Skipped.";
            $hasil['date'] = date('Y-m-d H:i:s');
            error_reporting($err);
            die(json_encode($hasil));
        }
        $ambil = $LINK;
        try {
            $page = get_data($LINK);
        } catch (Exception $e) {
            $hasil['error'] = 11;
            $hasil['msg'] = "Error loading page. {$e}";
            $hasil['date'] = date('Y-m-d H:i:s');
            error_reporting($err);
            die(json_encode($hasil));
        }
    } else {
        if ($retry >= $RETRY) {
            // skip page
            $dbh->query("update pages set done=1 where pageid={$rst['PAGEID']}");
            $hasil['id'] = $CHAPTERID;
            $hasil['pages'] = current($dbh->query("select count(pageid) from pages where chapterid={$CHAPTERID}")->fetch());
            $hasil['done'] = current($dbh->query("select count(pageid) from pages where chapterid={$CHAPTERID} and done=1")->fetch());
            $hasil['err'] = 0;
            $hasil['msg'] = "Skipped.";
            $dbh->query("update chapters set DONECOUNT={$hasil['done']} where chapterid={$CHAPTERID}");
            $hasil['date'] = date('Y-m-d H:i:s');
            error_reporting($err);
            die(json_encode($hasil));
        }
        $ambil = $rst['LINK'];
        try {
            $page = get_data($rst['LINK']);
        } catch (Exception $e) {
            $hasil['error'] = 12;
            $hasil['msg'] = "Error loading page. {$e}";
            $hasil['date'] = date('Y-m-d H:i:s');
            error_reporting($err);
            die(json_encode($hasil));
        }
    }

    if ($page=="") {
        $hasil['error'] = 13;
        $hasil['msg'] = "Empty page when loading {$ambil}.";
        $hasil['date'] = date('Y-m-d H:i:s');
        error_reporting($err);
        die(json_encode($hasil));
    }
    
    //$page = file_get_contents("tes.html");
    //file_put_contents("tes.html", $page);

    $dom = new DOMDocument;
    $dom->loadHTML($page);
    
    $curpage=0;
    $strpage="";
    
    // isi pages
    $st1 = $dbh->prepare("insert into pages (pagenum, chapterid, link, done) values (?,?,?,0)");
    $sels = $dom->getElementsByTagName("select");
    $scount = 0;
    
    foreach ($sels as $sel) {
        if ($sel->getAttribute("name")=="page" && $scount==0) {
            $opts = $sel->getElementsByTagName("option");
            foreach($opts as $opt) {
                $pnum = $opt->nodeValue;
                $plink = $BASE."manga/{$loc}/{$cloc}/{$pnum}";
                $st1->bindParam(1, $pnum);
                $st1->bindParam(2, $CHAPTERID);
                $st1->bindParam(3, $plink);
                try {
                    $st1->execute();
                    if ($opt->hasAttribute('selected')) $strpage = $pnum;
                } catch (Exception $e) {
                    //
                }
            }
            $scount++;
        }
    }

    $PAGES = current($dbh->query("select count(pageid) from pages where chapterid={$CHAPTERID}")->fetch());
    $dbh->query("update chapters set pages={$PAGES} where chapterid={$CHAPTERID}");
        
    if ($rst == false) {
        if ($strpage=="") {
            echo "NO SELECTED PAGE";
        }
        $curpage = current($dbh->query("select pageid from pages where chapterid={$CHAPTERID} and pagenum='{$strpage}'")->fetch());
    } else {
        $curpage=$rst['PAGEID'];
    }
    //echo "curpage : {$curpage} \n chapter_id: {$CHAPTERID}\n pagenum = {$strpage} \n";
    $IMGLINK = "";
    
    $domimgs = $dom->getElementsByTagName("img");
    foreach($domimgs as $domimg) {
        if ($domimg->getAttribute("class")=="picture")
            $IMGLINK = $BASE."manga/".trim($domimg->getAttribute("src"));
            break;
    }
    //$IMGLINK = current($dbh->query("select imglink from pages where pageid={$curpage}")->fetch());
    $qry = "update pages set imglink=? where pageid=".$curpage;
    $st2 = $dbh->prepare($qry);
    $st2->bindParam(1, $IMGLINK);
    $st2->execute();

    try {
        //echo "--{$IMGLINK}--";
        $imgf = get_data(myEncode($IMGLINK));
    } catch (Exception $e) {
        $hasil['error'] = 9;
        $hasil['msg'] = "Error loading image. ".$e;
        $hasil['date'] = date('Y-m-d H:i:s');
        error_reporting($err);
        die(json_encode($hasil));
    }
    
    if (strlen($imgf)<20) {
        $hasil['error'] = 19;
        $hasil['msg'] = "Error image size from {$IMGLINK}.";
        $hasil['date'] = date('Y-m-d H:i:s');
        error_reporting($err);
        die(json_encode($hasil));
    }
    
    $qry = "update pages set img=?, done=1 where pageid=".$curpage;
    $stt = $dbh->prepare($qry);
    $stt->bindParam(1, $imgf);
    $stt->execute();
    
    $dbh->query("update pages set IMGSIZE=length(hex(img))/2 where pageid={$curpage}");
    $count = current($dbh->query("select count(pageid) from pages where chapterid={$CHAPTERID} and done=1")->fetch());
    $dbh->query("update chapters set DONECOUNT={$count} where chapterid={$CHAPTERID}");
    
    $hasil['error'] = 0;
    $hasil['msg'] = "Success.";
    $hasil['id'] = $CHAPTERID;
    $hasil['pages'] = $PAGES;
    $hasil['done'] = $count;
    $hasil['date'] = date('Y-m-d H:i:s');
   
    error_reporting($err);
    die(json_encode($hasil));
?>
    