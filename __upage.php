<?php
    header('Access-Control-Allow-Headers: X-Requested-With, origin, content-type');
    header('Access-Control-Allow-Origin: *');

    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        die("{$fname} not found in filesystem.\nDatabase file not found.");
    }

    $chapid = isset($_POST['chap']) ? $_POST['chap'] : 0;
    $act = isset($_POST['act']) ? strtolower($_POST['act']) : "";
    $lp = isset($_POST['last']) ? $_POST['last'] : "";
    $pageid = isset($_POST['page']) ? $_POST['page'] : 0;
    
    require("db.php");

    $strcon = array();
    $strcon["DB_DSN"] = "sqlite:".$fname;
    $strcon["DB_USER"] = "";
    $strcon["DB_PASS"] = "";
    $dbh = new DB($strcon);

    if ($act=="last") {
        try {
            if ($lp != ""){
                $ctr = $dbh->run("select count(fkey) from manga where fkey='LAST_PAGE'")->fetchColumn();
                if ($ctr==0) $dbh->run("insert into manga (fkey, fvalue) values ('LAST_PAGE','0')");
                
                $lp = intval($lp)+1;
                //$pid = $dbh->run("select pageid from pages where chapterid=? and pagenum=?", array($chapid, $lp))->fetchColumn();
                //$dbh->run("update manga set fvalue=? where fkey='LAST_PAGE'",array($pid));
                $dbh->run("update manga set fvalue=(select pageid from pages where chapterid=? and pagenum=?) where fkey='LAST_PAGE'",array($chapid, $lp));
            }
        } catch(Exception $e) {
            error_log($e->getMessage());
        }

        header('Content-Type: application/json');
        echo json_encode(array('status'=>'ok','chap'=>$chapid, 'idx'=>$lp,'date'=>date('Y-m-d H:i:s')));
    }
    if ($act=="next"){
        $sql = "select coalesce(min(pageid),0) pgid from pages where pagenum='1' and chapterid=(select min(chapterid) from chapters where chapterid>(select chapterid from pages where pageid=?))";
        $nid = $dbh->run($sql, array($pageid))->fetchColumn();

        header('Content-Type: application/json');
        if (intval($nid)>0) {
            echo json_encode(array('nextid'=>intval($nid), 'msg'=>'OK'));
        } else {
            echo json_encode(array('nextid'=>0, 'msg'=>'No next chapter.'));
        }
    }
    if ($act=="prev"){
        $sql = "select coalesce(max(pageid),0) pgid from pages where chapterid=(select max(chapterid) from chapters where chapterid<(select chapterid from pages where pageid=?))";
        $nid = $dbh->run($sql, array($pageid))->fetchColumn();

        header('Content-Type: application/json');
        if (intval($nid)>0) {
            echo json_encode(array('nextid'=>intval($nid), 'msg'=>'OK'));
        } else {
            echo json_encode(array('nextid'=>0, 'msg'=>'No next chapter.'));
        }
    }
?>