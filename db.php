<?php
class DB{
	public $conn;
	public $error;
	public $affectedRows;
	
	public function __construct($P) {
		$this->conn = null;
		
        try{
			$opt  = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => FALSE,
            );
            $this->conn = new PDO($P["DB_DSN"], $P["DB_USER"], $P["DB_PASS"], $opt);
        }catch(PDOException $exception){
            $this->error = "Connection error: " . $exception->getMessage();
        }
	}

    public function __call($method, $args)
    {
        return call_user_func_array(array($this->conn, $method), $args);
    }

    public function run($sql, $args = array())
    {
		$this->affectedRows = 0;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute($args);
		$this->affectedRows = $stmt->rowCount();
        return $stmt;
    }

	public function loadToModel($obj){
		$sql = "select * from ".$obj->Table_Name." where id=?";
		$param = array($obj->id);
		$rows = $this->run($sql, $param)->fetchAll();
		$obj->load($rows[0]);
		return $this;
	}

	public function loadToTable($class, $sql, $param){
		$rows = $this->run($sql, $param)->fetchAll();
		$hasil = array();
		foreach ($rows as $row){
			$hasil[] = new $class($row);
		}
		return $hasil;
	}
	
	static function generateFormInsertQuery($tbl,$prm,$exclude){
		$field = array();
		$value = array();
		$quest = array();
		foreach ($prm as $key => $val){
			if (substr($key, 0, 4) == "fld_"){
				if (!in_array(substr($key,0,4),$exclude)) {
					$field[] = substr($key,4);
					$quest[] = "?";
					$value[] = $val;
				}
			}
		}
		$hasil = array();
		$hasil["sql"] = "insert into {$tbl} (" . implode(",",$field) . ") values (" . implode(",",$quest) . ") ";
		$hasil["params"] = $value;
		return $hasil;
	}
	static function generateFormUpdateQuery($tbl,$prm,$exclude){
		$field = array();
		$value = array();
		$pk_fld = null;
		$pk_val = null;
		
		foreach($prm as $key => $val){
			if (substr($key,0,4) == "fld_"){
				if (!in_array(substr($key,0,4),$exclude)) {
					$field[] = substr($key,4)."=?";
					$value[] = $val;
				}
			}
			if (substr($key,0,4) == "key_"){
				$pk_fld = substr($key,4);
				$pk_val = $val;
			}
		}
		$value[] = $pk_val;

		$hasil = array();
		$hasil["sql"] = "update {$tbl} set ".implode(",",$field)." where {$pk_fld}=?";
		$hasil["params"] = $value;
		return $hasil;
	}
}
?>