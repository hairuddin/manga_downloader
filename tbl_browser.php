<?php $mga=isset($_GET["db"])?$_GET["db"]:""; ?>
<html>
<head>
    <title>Page Info</title>
    <style type="text/css">
        body { background-color: white; }
        #debug { position: fixed; bottom: 0px; left: 0px; right: 0px; font-family: monospace; font-size: 8pt; color: #999; z-index: 0; }
        .header { font-size: 16pt; font-weight: bold; }
        .tabel { border-collapse: collapse; width: 100%; cursor: default; z-index: 99; }
        .tabel thead tr td { font-family: sans-serif; font-size: 8pt; font-weight: bold; padding: 8px; text-align: center; border: 1px solid #bbb; }
        .tabel tbody tr td { font-size: 8pt; padding: 4px; font-family: sans-serif; border: 1px solid #bbb; }
        .tabel tbody tr td:nth-child(1) { width: 150px; text-align: right; padding-right: 8px; background-color: maroon; font-weight: bold; color: white; }
        /* .tabel tbody tr td:nth-child(2) { width: 150px; } */
        .tabel tbody tr td:last-child   { width: 100px; text-align: center; font-size:8pt; white-space: nowrap; }
        /* .tabel tbody tr:hover td:not(:first-child):not(:last-child) { background-color: tan; } */
        .tabel tbody tr:hover td:first-child { background-color: brown; }
        .tabel a { background-color: maroon; text-decoration: none; color: white; display: inline-block; border-radius: 3px; padding: 2px 4px; }
        .tabel a:hover { background-color: brown; color: yellow; }
        .tabel .inedit { font-size: 8pt; width: 100%; border: none; }
    </style>
    <script defer src="fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js"></script>
    <script type="text/javascript" src="jquery-2.2.0.min.js"></script>
    <script type="text/javascript" src="numeral.min.js"></script>
    <script type="text/javascript">
        var DB = "<?=$mga?>";
        var TABLE = "";
        var DATA;
        var fmt_num = "0,0";
        var READONLY = []; //["SERVER","LOCATION","LAST_CHAPTER","COUNT_FILE_SIZE","COUNT_CHAPTER","COUNT_PAGE","CREATOR","LAST_PAGE"];
        function debug(dbg){$("#debug").html(dbg);}
        function getMetaByName(nama)
        {
            var hasil = null;
            for (var x=0; x<DATA.meta.length; x++) {
                if (DATA.meta[x].name == nama)
                    hasil = DATA.meta[x];
            }
            return hasil;
        }
        function loadTable(tbname)
        {
            TABLE = tbname;
            tname = tbname+" where substr(FKEY,1,2)<>'__'";
            $.ajax({
                url: '__db.php',
                data: {
                    'db': encodeURIComponent(DB),
                    'tbl': tbname,
                    'cmd': 'open table'
                },
                type: "POST", cache: false,
                success: function(responseText, responseStatus, responseXML) {
                    //DATA = JSON.parse(responseText);
                    DATA = responseText;
                    if (DATA.error != '0') {
                        debug(DATA.trace);
                    } else {
                        //debug(responseText);
                        processData();
                    }
                }
            });
        }
        function processData()
        {
            var domh = document.getElementById("tblh");
            var domb = document.getElementById("tblb");
            while (domh.firstChild) { domh.removeChild(domh.firstChild); }
            while (domb.firstChild) { domb.removeChild(domb.firstChild); }

            var tr = document.createElement("TR");
            var td = tr.insertCell(-1); td.innerHTML = "NO.";
            for (var x=0; x<DATA.meta.length; x++) {
                td = tr.insertCell(-1);
                td.innerHTML = DATA.meta[x].name;
            }
            td = tr.insertCell(-1); td.innerHTML = "ACTION";
            //domh.appendChild(tr);

            if (DATA.count>0)
            {
                var ROWS = DATA.rows;
                for (var y=0; y<DATA.count; y++) {
                    tr = document.createElement("TR");
                    tr.id = "tr"+y;
                    fld = "";
                    //td = tr.insertCell(-1); td.innerHTML = (1+y)+".";
                    $.each(ROWS[y], function(key, value){
                        td = tr.insertCell(-1);
                        if ($.isNumeric(value)) {
                            td.innerHTML = numeral(value).format(fmt_num);
                        } else {
                            td.innerHTML = value;
                        }
                        if (fld=="") fld = value;
                    });
                    vfn = "javascript:edt("+y+")";
                    if (READONLY.indexOf(fld.toUpperCase())>=0) vfn = "javascript:alert(\"Readonly Field.\")";
                    td = tr.insertCell(-1); td.innerHTML = "<a href='"+vfn+"'><span class='fas fa-edit'></span> Edit</a> <a href='javascript:del("+y+")'><span class='fas fa-times-circle'></span> Delete</a>";
                    domb.appendChild(tr);
                }
            }
        }
        function edt(r)
        {
            var tr = document.getElementById("tr"+r);
            var td = tr.childNodes[1];
            var value = DATA.rows[r].FVALUE;
            td.innerHTML = "<input class='inedit' type='text' id='editor' name='fvalue' value='"+value+"' onblur='redt("+r+")'>";
            $("#editor").focus();
        }
        function redt(r)
        {
            var tr = document.getElementById("tr"+r);
            var td = tr.childNodes[1];
            var rvalu = DATA.rows[r].FVALUE;
            var value = td.childNodes[0].value;
            if (rvalu!=value) {
                if (confirm('Save changes?'))
                {
                    // save to db
                    DATA.rows[r].FVALUE = value;
                    $.ajax({
                        url: '__db.php',
                        data: {
                            'db': DB,
                            'tbl': TABLE,
                            'cmd': 'update var',
                            'row': r,
                            'key': DATA.rows[r].FKEY,
                            'nval' : value
                        },
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            var reply = responseText; //JSON.parse(responseText);
                            var nval = reply.nval;
                            if ($.isNumeric(nval)) nval = numeral(nval).format(fmt_num);
                            document.getElementById("tr"+reply.row).childNodes[1].innerHTML = nval;
                        }
                    });
                    value = "Saving...";
                } else {
                    value = rvalu;
                }
            }
            if ($.isNumeric(value)) value = numeral(value).format(fmt_num);
            td.innerHTML = value;
        }
        function del(r)
        {
            if (confirm("Delete data?")) {
                $.ajax({
                    url: '__db.php',
                    data: {
                        'db': DB,
                        'tbl': TABLE,
                        'cmd': 'del var',
                        'row': r,
                        'key': DATA.rows[r].FKEY
                    },
                    type: "POST", cache: false,
                    success: function(responseText, responseStatus, responseXML) {
                        //var reply = JSON.parse(responseText);
                        window.location.reload(true); // reload from server;
                    }
                });
            }
        }
        function add()
        {
            var ctr = DATA.rows.length;
            var tblb = document.getElementById("tblb");
            var tr = document.createElement("TR");
            var td;
            tr.id = "tr"+ctr;
            td = tr.insertCell(-1);
            td.innerHTML = "<input type='text' id='new_fkey' name='new_fkey' value='' placeholder='Input New Key' class='inedit'>";
            td = tr.insertCell(-1);
            td.innerHTML = "<input type='text' id='new_fval' name='new_fval' value='' placeholder='New Value' class='inedit'>";
            td = tr.insertCell(-1);
            td.innerHTML = "<a href='javascript:new_save()'><span class='fas fa-check-circle'></span> Save</a> <a href='javascript:new_cancel()'><span class='fas fa-ban'></span> Cancel</a>";
            tblb.appendChild(tr);
            $("#new_fkey").focus();
        }
        function new_cancel()
        {
            if (confirm("Discard changes?")) {
                var domb = document.getElementById("tblb");
                domb.removeChild(domb.lastChild);
            }
        }
        function new_save()
        {
            if (confirm("Save changes?")) {
                var nkey = $("#new_fkey").val();
                var nval = $("#new_fval").val();
                var domb = document.getElementById("tblb");
                var tr = domb.lastChild;
                tr.childNodes[0].innerHTML = nkey;
                tr.childNodes[1].innerHTML = nval;
                tr.childNodes[2].innerHTML = "Saving...";
                var new_row = {"FKEY": nkey, "FVALUE": nval};
                DATA.rows.push(new_row);
                $.ajax({
                    url: '__db.php',
                    data: {
                        'db': DB,
                        'tbl': TABLE,
                        'cmd': 'new var',
                        'nkey' : nkey,
                        'nval' : nval
                    },
                    type: "POST", cache: false,
                    success: function(responseText, responseStatus, responseXML) {
                        //var reply = JSON.parse(responseText);
                        reply = responseText;
                        var ctr = DATA.rows.length-1;
                        var tr = document.getElementById("tr"+ctr);
                        tr.childNodes[0].innerHTML = reply.nkey.toUpperCase();
                        tr.childNodes[1].innerHTML = DATA.rows[DATA.rows.length-1].FVALUE;
                        //tr.childNodes[2].innerHTML = "<a href='javascript:edt("+ctr+")'><span class='icon-edit'></span> Edit</a>";
                        tr.childNodes[2].innerHTML = "<a href='javascript:edt("+ctr+")'><i class='fas fa-edit'></i> Edit</a> <a href='javascript:del("+ctr+")'><span class='fas fa-times-circle'></span> Delete</a>";
                    }
                });
            }
        }

        $(document).ready(function(){
            loadTable('MANGA');
        });
    </script>
</head>
<body>
    <div class="header">INFORMATION <a href="javascript:add()"><i class="fas fa-plus-circle"></i></a></div>
    <table class="tabel">
        <thead id="tblh"></thead>
        <tbody id="tblb"></tbody>
    </table>
    <div style="font-family: sans-serif; font-size: 10pt;">
    <ul>
        <li>MANGA_NAME = Judul manga</li>
        <li>CATEGORIES = Kategori/Genre manga</li>
        <li>STATUS = Isi dengan "COMPLETED" bila sudah selesai, selain itu dianggap masih bersambung.</li>
    </ul>
    </div>
    <div id="debug"></div>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        $("body").keydown(function(event){
            if (event.which == 27) {
                // esc  {close reader}
                window.parent.dlgClose();
                event.preventDefault();
            }
        });
   });
</script>
