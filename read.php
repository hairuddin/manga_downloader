<?php
    ini_set('max_execution_time', 300);
    $err = error_reporting();
    //error_reporting(0);

    $dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";
    $chapter = isset($_GET['ch']) ? $_GET['ch'] : "^_^";
    $schap = isset($_GET['str']) ? $_GET['str'] : "^_^";
    $vidx = isset($_GET['page']) ? $_GET['page'] : 0;
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    $dbfile = "__data/{$dbname}.mga";

    if (!file_exists($dbfile)) {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.<br>{$dbfile}";
        exit();
    }

    $dbh = new PDO("sqlite:{$dbfile}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    try {
        $dbh->query("CREATE INDEX IDX_PAGES_CHAPTER ON PAGES (CHAPTERID ASC)");
    } catch(Exception $e) {
        // do nothing
    }
    
    $lastchap = current($dbh->query("select coalesce(count(FValue),0) from MANGA where FKey='LAST_CHAPTER'")->fetch());
    if ($vidx!=0) {
        $chapter = current($dbh->query("select chapterid from pages where pageid={$vidx}")->fetch());
    } else {
        if ($chapter == "^_^") {
            $vidx = current($dbh->query("select coalesce(min(FValue),'0') from MANGA where FKey = 'LAST_PAGE'")->fetch());
            if ($vidx) {
                $chapter = current($dbh->query("select chapterid from pages where pageid={$vidx}")->fetch());
            } else {
                $chapter = current($dbh->query("select chapterid from chapters order by chapterid limit 1")->fetch());
            }
        } else {
            $lastchap = 1;
        }
    }
    if ($lastchap==0) {
        $dbh->query("insert into MANGA (FKey, FValue) values ('LAST_CHAPTER','{$chapter}')");
    } else {
        $dbh->query("update MANGA set FValue = '{$chapter}' where FKey = 'LAST_CHAPTER'");
    }

    if ($schap == "^_^") {
        $schap = current($dbh->query("select chapter from chapters where chapterid={$chapter}")->fetch());
    }

    $qry = "select PAGEID, PAGENUM from pages where chapterid={$chapter} and done=1 order by pageid";
    $rows = $dbh->query($qry);
    $pages = array();
    $pnums = array();
    $cbx = "<select id='cbxpage' onchange='load(this.value)'>";
    $cbn = 0;
    foreach ($rows as $row) {
        $pages[] = $row['PAGEID'];
        //$pnums[] = $row['PAGENUM'];
        $cbx .= "<option value='{$cbn}'>{$row['PAGENUM']}</option>";
        $cbn++;
    }
    $arr = implode(",", $pages);
    $cbx .= "</select>";
?><html>
    <head>
        <title>Web Reader</title>
        <link rel="stylesheet" type="text/css" href="fa/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="default.css">
        <script type="text/javascript" src="jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="jquery-ui/jquery-ui.min.js"></script>
        <script type="text/javascript" src="jquery.ui.touch-punch.min.js"></script>
        <script type="text/javascript" src="default.js"></script>
        <script type="text/javascript">
            var DB = "<?php echo $dbname; ?>";
            var PAGES = [<?php echo $arr; ?>];
            var CHP = "<?php echo $chapter; ?>";
            var IDX = 0;
            var VIDX = <?php echo $vidx; ?>;
            var MAX = PAGES.length;
            var Threshold = 50;
            var w1;
            var w2;
            var h1;
            var h2;
            var rw2;
            var rh2;

            function remap(newsrc) {
                w1 = $("#chp").innerWidth(); //window.innerWidth;
                h1 = $("#chp").innerHeight();

                if (newsrc.length>0) {
                    $$("preview").style.width = 'auto';
                    $$("preview").style.height = 'auto';
                }
                var iw = $$("preview").width;
                var ih = $$("preview").height;
                var mapp = $$("nprev");
                var mapn = $$("nnext");

                var px1 = 0;
                var py1 = 0;
                var px2 = Math.floor(iw/10);
                var py2 = ih;

                var nx1 = iw-Math.floor(iw/10);
                var ny1 = 0;
                var nx2 = iw;
                var ny2 = ih;

                var pcoord = px1+","+py1+","+px2+","+py2;
                var ncoord = nx1+","+ny1+","+nx2+","+ny2;

                mapp.coords = pcoord;
                mapn.coords = ncoord;

                w2 = iw;
                h2 = ih;
                if (newsrc.length>0) {
                    rw2 = iw;
                    rh2 = ih;
                    if (w1>w2) {
                        $$("preview").style.left = (w1-w2)/2;
                    } else {
                        $$("preview").style.left = 0;
                    }
                    $$("preview").style.top = 0;
                }

                // fit (reduce) image preview to window width
                iw = $$("preview").width;
                ih = $$("preview").height;
                while (iw>w1) {
                    zoom_out();
                    iw = $$("preview").width;
                }
            }

            function load(num) {
                $$("preview").style.opacity = 0.4;
                $$("cbxpage").value = parseInt(num);
                $$("preview").src = "file.php?db="+encodeURIComponent(DB)+"&id="+PAGES[parseInt(num)];
                IDX = parseInt(num);
                $.ajax({
                    url: '__db.php',
                    data: {'db': DB, 'cmd':'update var', 'key': 'LAST_PAGE', 'nval': PAGES[IDX]},
                    type: "POST",
                    cache: false
                });
            }

            function next() {
                if (IDX == MAX-1) {
                    $.ajax({
                        url: '__getnextchapter.php',
                        data: {'db': DB, 'ch': CHP},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            newchap = parseInt(responseText);
                            if (newchap>0) {
                                window.location = "read.php?db="+encodeURIComponent(DB)+"&ch="+newchap;
                            } else {
                                alert("End of downloaded manga.");
                            }
                        }
                    });
                    $$("preview").style.opacity = 0.4;
                }
                var old = IDX;
                if (IDX<MAX-1) IDX = IDX+1;
                if (old!=IDX) load(IDX);
            }

            function prev() {
                var old = IDX;
                if (IDX>0) IDX = IDX-1;
                if (old!=IDX) {
                    load(IDX);
                }
            }

            function zoom_in() {
                h2 = h2*1.2;
                w2 = w2*1.2;
                if (h2>rh2*2) h2 = rh2*2;
                if (w2>rw2*2) w2 = rw2*2;
                $$("preview").style.width = w2;
                $$("preview").style.height = h2;
                remap("");
                if (w1>w2) {
                    ex = (w1-w2)/2;
                    return true;
                }
                return false;
            }

            function zoom_out() {
                h2 = h2*0.8;
                w2 = w2*0.8;
                if (h2<rh2*0.3) h2 = rh2*0.3;
                if (w2<rw2*0.3) w2 = rw2*0.3;
                $$("preview").style.width = w2;
                $$("preview").style.height = h2;
                remap("");
                if (w1>w2) {
                    ex = (w1-w2)/2;
                    return true;
                }
                return false;
            }

            function do_zoom(tipe) {
                var ubah = false;
                var ye = $("#preview").position().top;
                var ex = $("#preview").position().left;
                if (tipe=="in") {
                    ubah = zoom_in();
                } else {
                    ubah = zoom_out();
                }
                if (ubah) {
                    $$("preview").style.top = ye;
                    $$("preview").style.left = ex;
                }
            }

            $(document).ready(function(){
                $("#ctr").html(PAGES.length);
                if (VIDX!=0) IDX=PAGES.indexOf(VIDX);
                load(IDX);

                window.parent.dlgCaption("<?php echo $schap; ?>");

                $("#preview").draggable({
                    drag: function(event, ui) {
                        //$("#tex").html(w1+','+w2);
                        if (w1>w2) {
                            ui.position.left = (w1-w2)/2;
                        } else {
                            ui.position.left = Math.max(w1-w2-6, Math.min(0,ui.position.left));
                        }
                        if (h1>h2) {
                            ui.position.top = 0;
                        } else {
                            ui.position.top = Math.max(h1-h2-6,Math.min(0,ui.position.top));
                        }
                    }
                });

                $("#chp").on("mousewheel", function(event){
                    //console.log(event.deltaX, event.deltaY, event.deltaFactor);
                    var ye = $("#preview").position().top+(event.deltaY*event.deltaFactor);
                    if (ye>0) ye = 0;
                    if (h1>h2) {
                        ye = 0;
                    } else {
                        if (ye<(h1-h2-6)) ye = h1-h2-6;
                    }
                    $$("preview").style.top = ye;
                    event.preventDefault();
                });

                $("body").keydown(function(event){
                    var ubah = false;
                    var ye = $("#preview").position().top;
                    var ex = $("#preview").position().left;
                    //alert(event.which);
                    if ((event.which == 32) || (event.which == 34)) {
                        // space key or page down  {scroll down}
                        ye = ye-h1;
                        if (ye<(h1-h2-6)) ye = h1-h2-6;
                        ubah = true;
                    } else if (event.which == 33) {
                        // page up  {scoll up}
                        ye = ye+h1;
                        if (ye>0) ye = 0;
                        ubah = true;
                    } else if (event.which == 35) {
                        // end  {scroll bottom}
                        ye = h1-h2-6;
                        ubah = true;
                    } else if (event.which == 36) {
                        // home  {scroll top}
                        ye = 0;
                        ubah = true;
                    } else if (event.which == 37) {
                        // left arrow  {scroll left}
                        if (w2>w1) {
                            ex = ex+Threshold;
                            if (ex>0) ex = 0;
                        }
                        ubah = true;
                    } else if (event.which == 39) {
                        // right arrow  {scroll right}
                        if (w2>w1) {
                            ex = ex-Threshold;
                            if (ex<w1-w2-6) ex = w1-w2-6;
                        }
                        ubah = true;
                    } else if (event.which == 38) {
                        // up key  {move up}
                        ye = ye+Threshold;
                        if (ye>0) ye = 0;
                        ubah = true;
                    } else if (event.which == 40) {
                        // down key  {move down}
                        ye = ye-Threshold;
                        if (ye<(h1-h2-6)) ye = h1-h2-6;
                        ubah = true;
                    } else if ((event.which == 61) || (event.which == 187)) {
                        // =  {zoom in}
                        ubah = zoom_in();
                    } else if ((event.which == 45) || (event.which == 189)) {
                        // -  {zoom out}
                        ubah = zoom_out();
                    } else if (event.which == 188) {
                        // , <  {prev page}
                        prev();
                    } else if (event.which == 190) {
                        // . >  {next page}
                        next();
                    } else if (event.which == 27) {
                        // esc  {close reader}
                        window.parent.dlgClose();
                    }
                    if (ubah) {
                        $$("preview").style.top = ye;
                        $$("preview").style.left = ex;
                        event.preventDefault();
                    }
                });
            });
        </script>
    </head>
    <body>
        <map name="navmap">
            <area id="nprev" shape="rect" coords="0,0,100,100" alt="Prev" href="javascript:prev()" style="cursor:w-resize">
            <area id="nnext" shape="rect" coords="100,0,200,100" alt="Next" href="javascript:next()" style="cursor:e-resize">
        </map>
        <div>
            <br><div><b>Chapter : <?php echo $schap; ?></b> ; Page <?=$cbx?> of [<span id="ctr"></span>] <a href="javascript:prev()">[ &lt;&lt; Prev Page ]</a> <a href="javascript:next()">[ Next Page &gt;&gt; ]</a><span id='tex'></span></div>
            <br>
        </div>
        <div id="chp" style="text-align: center; position: fixed; top: 60px; left:5px; right:5px; bottom:5px; overflow: hidden;">
            <img id="preview" src="" onload="this.style.opacity = 1; remap(this.src);" style="border-style: solid; border-width:3px; border-color:black; position: absolute;" usemap="#navmap">
        </div>
        <div id="debug"></div>
        <div id="navi">&nbsp;
            <a href="javascript:do_zoom('in')"><i class="icon-plus-sign icon-2x"></i></a>
            <a href="javascript:do_zoom('out')"><i class="icon-minus-sign icon-2x"></i></a>
            <a href="javascript:prev()"><i class="icon-chevron-left icon-2x"></i></a>
            <a href="javascript:next()"><i class="icon-chevron-right icon-2x"></i></a>
        </div>
    </body>
</html>
