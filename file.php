<?php
    ini_set('max_execution_time', 300);
    $err = error_reporting();
    error_reporting(0);

    $dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";
    $pageid = isset($_GET['id']) ? $_GET['id'] : "^_^";
    $resw = isset($_GET['resw']) ? $_GET['resw'] : "";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    $fname = "__data/{$dbname}.mga";

    if (!file_exists($fname)) {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }

    require_once "functions.php";

    $dbh = new PDO("sqlite:{$fname}");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $qry = "select * from pages where pageid={$pageid}";
    $row = $dbh->query($qry)->fetch();

    if ($row["IMGSIZE"]=="") $dbh->query("update pages set IMGSIZE=length(hex(img))/2 where pageid={$pageid}");

    $arr = explode(".", $row['IMGLINK']);
    $ext = $arr[count($arr)];
    $condis = "{$dbname}-{$pageid}-page.".$ext;
    $mime = mime_content_type($condis);
    $img = $row['IMG'];

    if (ImageCreateFromString($img) === false)
    {
        // image is corrupted
        $img = file_get_contents("broken_image.gif");
        $mime = mime_content_type("broken_image.gif");
        $condis = "{$dbname}-{$pageid}-broken-page.gif";
    }

    if ($resw != "") {
        $ext = "jpg";
        $mime = mime_content_type("page.jpg");
        $img = resizeImageString($img, $resw);
    }

    $condis = "{$dbname}-{$pageid}-page.".$ext;
    header('Content-Disposition: filename="'.$condis.'"');
    header("Content-Type: ".$mime);
    header("Content-Length: ".strlen($img));
    echo $img;
?>