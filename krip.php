<html>
<head>
    <title>Reader</title>
    <link rel="stylesheet" type="text/css" href="fa/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="default.css">
    <script type="text/javascript" src="jquery-2.2.0.min.js"></script>
    <script type="text/javascript" src="jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="numeral.min.js"></script>
    <script type="text/javascript" src="default.js"></script>
    <style type="text/css">
        pre { font-size: 12pt; }
    </style>
    <script type="text/javascript">
        var IDX = -1;
        var files = [
            '__chapters.php',
            '__createdb.php',
            '__db.php',
            '__getnextchapter.php',
            '__infodb.php',
            '__listdb.php',
            '__skipchapter.php',
            'download.php',
            'file.php',
            'functions.php',
            'index.php',
            'manga.php',
            'pages.php',
            'read.php',
            'tbl_browser.php',
            '__servers/komikid/__readchapters.php',
            '__servers/komikid/__readpageauto.php',
            '__servers/komikid/vars.php',
            '__servers/pecintakomik/__readchapters.php',
            '__servers/pecintakomik/__readpageauto.php',
            '__servers/pecintakomik/vars.php',
            '__servers/taadd/__readchapters.php',
            '__servers/taadd/__readpageauto.php',
            '__servers/taadd/vars.php',
            '__servers/comicextra/__readchapters.php',
            '__servers/comicextra/__readpageauto.php',
            '__servers/comicextra/vars.php',
            '__servers/readcomics/__readchapters.php',
            '__servers/readcomics/__readpageauto.php',
            '__servers/readcomics/vars.php'
        ];
        function listFile() {
            for(var x=0; x<files.length; x++) {
                document.write('o <a href="javascript:proses('+x+')">'+files[x]+'</a> ');
                document.write('[<span id="sts_'+x+'">status</span>]');
                document.writeln('');
            }
        }
        function proses(idx) {
            IDX = idx;
            $.ajax({
                url: 'kr1p.php',
                data: {'krip': files[idx], 'nama': $('#regs').val()},
                type: "GET", cache: false,
                success: function(responseText, responseStatus, responseXML) {
                    $('#sts_'+IDX).html(responseText);
                }
            });
        }
    </script>
</head>
<body><pre><b>File Locker;</b>
Registered to <input type="text" id="regs" name="regs" size="31">
<script type="text/javascript">
    listFile();
</script>
</pre>
</body>
</html>