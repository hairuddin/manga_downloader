<?php
function genThumbMIME64($data, $width) {
    $hasil = "data:image/png;base64,";
    
    $nw = $width;
    $nh = $width;
    $isize = getimagesizefromstring($data);
    $iw = $isize[0];    //Images width
    $ih = $isize[1];    //Images height

    $simg = imagecreatefromstring($data);
    $terus = true;

    if ($terus) {
        $dimg = imagecreatetruecolor($nw, $nh);
        if ($iw>$ih) {
            $wh = $ih;
            $sx = ($iw-$ih)/2;
            $sy = 0;
        } else {
            $wh = $iw;
            $sx = 0;
            $sy = ($ih-$iw)/2;
        }

        imagecopyresampled($dimg,$simg,0,0,$sx,$sy,$nw,$nh,$wh,$wh);
        ob_start();
        imagepng($dimg);
        $binthumb = ob_get_clean();
        $hasil .= base64_encode($binthumb);
    }
    return $hasil;
}

$dbn = isset($_GET["db"]) ? $_GET["db"] : "-";
$id = isset($_GET["id"]) ? $_GET["id"] : "-";
$dbdir = dirname(__file__);
$separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
if ($id=="-" || $dbn=="-") {
    header('HTTP/1.0 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "The page that you have requested could not be found.";
    exit();
}
$db = new PDO("sqlite:{$dbdir}{$separator}..{$separator}{$dbn}.life");
$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

$sql = "SELECT a.*, b.itembin, b.compress_type, b.hash FROM files a inner join items b on a.itemid=b.itemid where a.fileid=?";
$sth = $db->prepare($sql);
$sth->execute(array($id));

if ($data=$sth->fetch()) {
    $ctype = $data['CONTENTTYPE'];
    
    $x = explode(".", $data['FILENAME']);
    $ext = $x[count($x)-1];
    $icon = "../ext/__none__128.png";
    if (file_exists("../ext{$separator}{$ext}128.png"))
        $icon = "../ext/{$ext}128.png";
} else {
    header('HTTP/1.0 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "The page that you have requested could not be found.";
    exit();
}
?>
<html>
    <head>
        <title>Image Viewer</title>
        <style type="text/css">
            body { cursor: default; overflow: auto; background-color: silver; }
            .tgs { padding: 3px; margin: 2px; border: 1px solid; border-radius:3px; display: inline-block; }
            .tgs:hover { background-color: yellow; }
            .inpt { width: 75%; padding: 1px; }
            .thumb { background:url(<?=$icon?>); position:absolute; left:10px; top:10px; width:128px; height:128px; display:block; border-style:solid; border-width:5px; border-color:silver; border-radius: 5px; box-shadow: silver 0px 0px 12px; }
            .thumb:hover { background:url(<?=genThumbMIME64($data['ITEMBIN'],128)?>); border-color:black; box-shadow: #000 0px 0px 12px; }
            a { text-decoration: none; color: maroon; }
            .icon { position:absolute; left:10px; top:10px; }
            ::-webkit-scrollbar { width: 8px; height: 8px; }
            ::-webkit-scrollbar-track { -webkit-box-shadow: inset 0 0 6px rgba(200,200,200,0.8); }
            ::-webkit-scrollbar-thumb { background: rgba(155,155,155,0.3); -webkit-box-shadow: inset 0 0 6px rgba(200,200,200,0.5); }
            ::-webkit-scrollbar-thumb:window-inactive { background: rgba(155,155,155,0.4); }
        </style>
        <script type="text/javascript">
            function $(id) { return document.getElementById(id); }
            function zoomIn() {
                $('view').style.width = '';
                $('view').style.height = '';
            }
            function zoomOut(mode) {
                var ww = window.innerWidth-20;
                var wh = window.innerHeight-20;
                if (mode=="W") {
                    $('view').style.width = ww+'px';
                } else {
                    $('view').style.height = wh+'px';
                }
            }
            function fix() {
                var MODE = "W";
                var v = $('view');
                zoomIn();
                var iw = v.offsetWidth/v.offsetHeight;
                var ih = v.offsetHeight/v.offsetWidth;
                var ww = window.innerWidth/window.innerHeight;
                var wh = window.innerHeight/window.innerWidth;
                var iwww = iw/ww;
                var ihwh = ih/wh;
                if (iwww>=ihwh) { MODE = "W"; } else { MODE = "H"; }
                zoomOut(MODE);
            }
            var XID = <?=$id?>;
            var XDB = "<?=$dbn?>";
            function doPrev() {
                var nid = window.parent.getPrevID(XID);
                    if (nid > -1) {
                    window.location = "info.php?db="+XDB+"&id="+nid;
                }
            }
            function doNext() {
                var nid = window.parent.getNextID(XID);
                if (nid > -1) {
                    window.location = "info.php?db="+XDB+"&id="+nid;
                }
            }
            function isiNext() {
                var nid = window.parent.getNextID(XID);
                $("next").value = nid;
            }
            function toggle(ids) {
                $(ids).disabled = !$(ids).disabled;
            }
            function appends(id,teks) {
                var t = $(id).value;
                if (t.length>0) t += " ";
                t += teks;
                $(id).value = t;
            }
            function appendx(id,teks) {
                $(id).value = teks;
            }
        </script>
    </head>
    <body>
        <form action="info_post.php" method="post" onsubmit="isiNext()">
        <input type="hidden" id="db" name="db" value="<?=$dbn?>">
        <input type="hidden" id="fileid" name="fileid" value="<?= $data['FILEID'] ?>">
        <input type="hidden" id="next" name="next" value="">
        <pre style="margin: 10px 10px 10px 160px;">
FileID      : <input class="inpt" type="text" id="fid" name="fid" value="<?= $data['FILEID'] ?>" disabled>
<a href="javascript:toggle('filename')">File Name</a>   : <input class="inpt" type="text" id="filename" name="filename" value="<?= $data['FILENAME'] ?>" disabled>
<a href="javascript:toggle('contenttype')">Content Type</a>: <input class="inpt" type="text" id="contenttype" name="contenttype" value="<?= $data['CONTENTTYPE'] ?>" disabled>
Size        : <input class="inpt" type="text" id="size" name="size" value="<?= number_format($data['SIZE']) ?>" disabled>
Note        : <input class="inpt" type="text" id="note" name="note" value="<?= $data['NOTE'] ?>">
Tag         : <input class="inpt" type="text" id="tag" name="tag" value="<?= $data['TAG'] ?>">
Bintang     : <input class="inpt" type="text" id="bintang" name="bintang" value="<?= $data['BINTANG'] ?>">
<a href="javascript:toggle('itemid')">ItemID</a>      : <input class="inpt" type="text" id="itemid" name="itemid" value="<?= $data['ITEMID'] ?>" disabled>
<a href="javascript:toggle('folderid')">FolderID</a>    : <input class="inpt" type="text" id="folderid" name="folderid" value="<?= $data['FOLDERID'] ?>" disabled>
Compression : <input class="inpt" type="text" id="compress" name="compress" value="<?= $data['COMPRESS_TYPE'] ?>" disabled>
Hash        : <input class="inpt" type="text" id="hash" name="hash" value="<?= $data['HASH'] ?>" disabled>

Tags        : <a class="tgs" href="javascript:appends('tag','edun+')">edun+</a> <a class="tgs" href="javascript:appends('tag','edun')">edun</a> <a class="tgs" href="javascript:appends('tag','edun-')">edun-</a> <a class="tgs" href="javascript:appends('tag','esi+')">esi+</a> <a class="tgs" href="javascript:appends('tag','esi')">esi</a> <a class="tgs" href="javascript:appends('tag','esi-')">esi-</a> <a class="tgs" href="javascript:appends('tag','hace')">hace</a> <a class="tgs" href="javascript:appends('tag','hace-')">hace-</a> <a class="tgs" href="javascript:appends('tag','aktion+')">aktion+</a> <a class="tgs" href="javascript:appends('tag','aktion')">aktion</a>
              <a class="tgs" href="javascript:appends('tag','anim')">anim</a> <a class="tgs" href="javascript:appends('tag','favo')">favo</a> <a class="tgs" href="javascript:appends('tag','public')">public</a> <a class="tgs" href="javascript:appends('tag','del')">del</a>

Bintang     : <a class="tgs" href="javascript:appendx('bintang','1')"> 1 </a> <a class="tgs" href="javascript:appendx('bintang','2')"> 2 </a> <a class="tgs" href="javascript:appendx('bintang','3')"> 3 </a> <a class="tgs" href="javascript:appendx('bintang','4')"> 4 </a> <a class="tgs" href="javascript:appendx('bintang','5')"> 5 </a> <a class="tgs" href="javascript:appendx('bintang','6')"> 6 </a> <a class="tgs" href="javascript:appendx('bintang','7')"> 7 </a> <a class="tgs" href="javascript:appendx('bintang','8')"> 8 </a> <a class="tgs" href="javascript:appendx('bintang','9')"> 9 </a>
              
              <input type="submit" value="Submit"><br>
              <input type="button" value="<< Prev" onclick="doPrev()"> <input type="button" value="Next >>" onclick="doNext()">
<?php
    if (substr($ctype,0,5)=="image") {
        echo "<a class='thumb' border='1' href='preview.php?db={$dbn}&id={$id}'></a>";
        //echo "<a href='preview.php?db={$dbn}&id={$id}'><img class='icon' src='{$icon}'></a><br>";
    } else { 
        if (substr($ctype,0,4)=="text") {
            echo "<a href='preview.php?db={$dbn}&id={$id}'><img class='icon' src='{$icon}'></a><br>";
        } else {
            echo "<img class='icon' src='{$icon}'><br>";
        }
    }
?>
        </pre>
        </form>
    </body>
    <script type="text/javascript">
        document.body.onkeydown = function(event){
            event = event || window.event;
            var keycode = event.charCode || event.keyCode;
            if(keycode === 27){
                window.parent.dlgClose();
            }
        }
    </script>
</html>
