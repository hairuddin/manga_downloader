<?php
$dbn = isset($_GET["db"]) ? $_GET["db"] : "-";
$id = isset($_GET["id"]) ? $_GET["id"] : "-";
$dbdir = dirname(__file__);
$separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
if ($id=="-" || $dbn=="-") {
    header('HTTP/1.0 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "The page that you have requested could not be found.";
    exit();
}
$db = new PDO("sqlite:{$dbdir}{$separator}..{$separator}{$dbn}.life");
$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

$sql = "SELECT a.* FROM files a where a.fileid=?";
$sth = $db->prepare($sql);
$sth->execute(array($id));

if ($data=$sth->fetch()) {
    $ctype = $data['CONTENTTYPE'];
} else {
    header('HTTP/1.0 404 Not Found');
    echo "<h1>404 Not Found</h1>";
    echo "The page that you have requested could not be found.";
    exit();
}
?>
<html>
    <head>
        <title>Image Viewer</title>
        <style type="text/css">
            body { margin: 0px; cursor: default; overflow: auto; text-align: center; }
            #close { position: fixed; right:5px; top:0px; cursor: pointer; opacity: 0.1; }
            #close:hover { opacity: 1; }
            #satu { position: fixed; top:0px; left:0px; cursor: pointer; opacity: 0.1; }
            #satu:hover { opacity: 1; }
            #dua { position: fixed; bottom:5px; left:0px; cursor: pointer; opacity: 0.1; }
            #dua:hover { opacity: 1; }
            #next { position: fixed; right:5px; bottom:5px; cursor: pointer; opacity: 0.1; }
            #next:hover { opacity: 1; }
            #prev { position: fixed; right:75px; bottom:5px; cursor: pointer; opacity: 0.1; }
            #prev:hover { opacity: 1; }
            #info { position: fixed; right:5px; bottom:30px; cursor: pointer; opacity: 0.1; }
            #info:hover { opacity: 1; }
            ::-webkit-scrollbar { width: 8px; height: 8px; }
            ::-webkit-scrollbar-track { -webkit-box-shadow: inset 0 0 6px rgba(200,200,200,0.8); }
            ::-webkit-scrollbar-thumb { background: rgba(155,155,155,0.3); -webkit-box-shadow: inset 0 0 6px rgba(200,200,200,0.5); }
            ::-webkit-scrollbar-thumb:window-inactive { background: rgba(155,155,155,0.4); }
            .dragme { position: relative; }
        </style>
        <script type="text/javascript">
            function $(id) { return document.getElementById(id); }
            var iWid, iHei
            var MODE = "W";
            function zoomIn() {
                $('view').style.width = '';
                $('view').style.height = '';
            }
            function zoomOut() {
                var ww = window.innerWidth;
                var wh = window.innerHeight;
                $('view').style.left = '';
                $('view').style.top = '';
                if (MODE=="W") {
                    $('view').style.width = ww+'px';
                } else {
                    $('view').style.height = wh+'px';
                }
            }
            function fix() {
                var v = $('view');
                zoomIn();
                iWid = v.offsetWidth;
                iHei = v.offsetHeight;
                var iw = v.offsetWidth/v.offsetHeight;
                var ih = v.offsetHeight/v.offsetWidth;
                var ww = window.innerWidth/window.innerHeight;
                var wh = window.innerHeight/window.innerWidth;
                var iwww = iw/ww;
                var ihwh = ih/wh;
                if (iwww<ihwh) { MODE = "H"; } else { MODE = "W"; }
                zoomOut();
            }
            var XID = <?=$id?>;
            var XDB = "<?=$dbn?>";
            function doPrev() {
                var nid = window.parent.getPrevID(XID);
                    if (nid > -1) {
                    window.location = "preview.php?db="+XDB+"&id="+nid;
                }
            }
            function doNext() {
                var nid = window.parent.getNextID(XID);
                if (nid > -1) {
                    window.location = "preview.php?db="+XDB+"&id="+nid;
                }
            }
            function doInfo() {
                window.location = "info.php?db="+XDB+"&id="+XID;
            }
        </script>
    </head>
    <body>
        <?php if (substr($ctype,0,5)=="image") {?>
        <img id="view" class="dragme" src="file.php?db=<?=$dbn?>&id=<?=$id?>" onload="fix()">
        <img id="satu" src="icon/zoomin.png" onclick="zoomIn()">
        <img id="dua" src="icon/zoomout.png" onclick="zoomOut()">
        <img id="close" src="icon/closebutton.png" onclick="window.parent.dlgClose();">
        <input type="button" value="Next >>" id="next" onclick="doNext()">
        <input type="button" value="<< Prev" id="prev" onclick="doPrev()">
        <input type="button" value="[i] Info" id="info" onclick="doInfo()">
        <?php } else {
        //echo file_get_contents("http://".$_SERVER["HTTP_HOST"].dirname($_SERVER['PHP_SELF'])."file.php?db={$dbn}&id={$id}");
        echo file_get_contents("http://".$_SERVER["HTTP_HOST"]."/file.php?db={$dbn}&id={$id}");
        }?>
    </body>
    <script type="text/javascript">
        document.body.onkeydown = function(event){
            event = event || window.event;
            var keycode = event.charCode || event.keyCode;
            if(keycode === 27){
                window.parent.dlgClose();
            }
        }
        window.parent.document.getElementById('cdlg').innerHTML = '<?=$data['FILENAME']?>';
    </script>
    <script type="text/javascript">
        function startDrag(e) {
            // determine event object
            if (!e) {
                var e = window.event;
            }

            // IE uses srcElement, others use target
            var targ = e.target ? e.target : e.srcElement;

            if (targ.className != 'dragme') {return};
            // calculate event X, Y coordinates
                offsetX = e.clientX;
                offsetY = e.clientY;

            // assign default values for top and left properties
            if(!targ.style.left) { targ.style.left='0px'};
            if (!targ.style.top) { targ.style.top='0px'};

            // calculate integer values for top and left 
            // properties
            coordX = parseInt(targ.style.left);
            coordY = parseInt(targ.style.top);
            drag = true;

            // move div element
                document.onmousemove=dragDiv;
                
            e.preventDefault();
            //return false;
        }
        function dragDiv(e) {
            if (!drag) {return};
            if (!e) { var e= window.event};
            var targ=e.target?e.target:e.srcElement;
            // move div element
            targ.style.left=coordX+e.clientX-offsetX+'px';
            targ.style.top=coordY+e.clientY-offsetY+'px';
            return false;
        }
        function stopDrag() {
            drag=false;
        }
        window.onload = function() {
            document.onmousedown = startDrag;
            document.onmouseup = stopDrag;
        }
    </script>
</html>