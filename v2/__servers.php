<?php
$dirs = array_filter(glob('../__servers/*'), 'is_dir');
$result = array();
foreach($dirs as $dir) {
    $svr = strtoupper(str_replace("../__servers/","",$dir));
    $stc = new stdClass();
    $stc->id = $svr;
    $stc->text = $svr;
    $result[] = $stc;
}
print json_encode($result);
?>
