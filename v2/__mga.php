<?php
$files = scandir("../__data");
$mds = array();
$result = array();

$dbh = new PDO("sqlite:list.db3", null, null);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$qry = "
    insert into mlist (title, server_key, server, db_name, last_update) 
    values (?,?,?,?, CURRENT_TIMESTAMP)
";
$stm = $dbh->prepare($qry);

$sql = "select fvalue from manga where fkey='SERVER'";

foreach ($files as $file) {
    $x = explode(".", $file);
    if ($x[count($x)-1]=="mga") {
        $mga = new PDO("sqlite:../__data/{$file}", null, null);
        $mga->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $fdb = substr($file, 0, strlen($file)-4);
        $error = "OK";
        try {
            $mgs = $mga->prepare($sql);
            $mgs->execute();
            $rws = $mgs->fetch();
            $svr = $rws[0];
            
            $mga->query("insert into MANGA (FKey, FValue) values ('LOCATION','{$fdb}')");
        
            $param = array(
                $fdb,
                $fdb,
                $svr,
                "{$fdb}.mga"
            );
            $stm->execute($param);
            $id = $dbh->lastInsertId();
            //$dbh->commit();
        } catch (PDOException $e) {
            $error = $e->getMessage();
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
        $mga = null;
        $mgs = null;
        echo("{$fdb} --&gt; {$error}<br>");
    }
}
//print json_encode($result);
?>
