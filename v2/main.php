<!DOCTYPE html>
<?php
require_once("functions.php");

$module = isset($_GET["m"]) ? $_GET["m"] : "list";
$title = isset($_GET["t"]) ? $_GET["t"] : "Manga List";
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>SQLife</title>
    <link rel="stylesheet" type="text/css" href="themes/black/easyui.css">
    <link rel="stylesheet" type="text/css" href="themes/icon.css">
    <link rel="stylesheet" type="text/css" href="themes/color.css">
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="jquery.easyui.min.js"></script>
    <style type="text/css">
        body   { font-family: sans-serif; background-color: #444; }
        #dlg    { overflow: auto; border:5px solid #000; border-radius: 5px 5px 3px 3px; background-color:#000; color:#fff;
                    font-family: cursive; font-size:10pt; box-shadow: 0px 0px 8px #000; z-index: 110; }
        #dlg div.caption { padding:3px; font-weight: bold; }
        .north { height:40px; color: black; background:#aaaaaa; padding:10px; margin:-20px 0px 0px 0px; overflow: hidden; }
        .west  { width:200px; padding:0px; }
        .east  { width:100px; padding:10px; text-align: center; }
        .south { height:20px; color: black; background:#aaaaaa; padding:5px; font-size: 10pt; text-align: center; }
        .bigbtn { width: 80px; margin: 2px 1px; }
        .mnubtn { width: 120px; margin: 5px; text-align:left; }
    	.mnugrp { padding: 10px; text-align: center; }
    	.full   { width: 100%; }
        
        .ftitle { font-size:14px; font-weight:bold; padding:5px 0; margin-bottom:10px; border-bottom:1px solid #ccc; }
		.fitem  { margin-bottom:5px; }
		.fitem label { display:inline-block; width:80px; }
		.fitem input { width:160px; }
        .btn-separator{
    		display:inline-block;
    		width:0;
    		height:22px;
    		border-left:1px solid #ccc;
    		border-right:1px solid #fff;
    		vertical-align:middle;}
        .spasi { display:inline-block; width:10px; }
    </style>
    <script type="text/javascript">
        var GETMODULE = "<?=$module?>";
        var GETTITLE = "<?=$title?>";
        /**
         * Number.prototype.format(n, x)
         * 
         * @param integer n: length of decimal
         * @param integer x: length of sections
         */
        Number.prototype.format = function(n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };
        String.prototype.paddingLeft = function (paddingValue) {
            return String(paddingValue + this).slice(-paddingValue.length);
        };
        function supportsLocalStorage() {
            try {
                return 'localStorage' in window && window['localStorage'] !== null;
            } catch (e) {
                return false;
            }
        }
        function getVar(key) {
            if (!supportsLocalStorage()) { return null; }
            return localStorage['SQLife__'+key];
        }
        function setVar(key, value) {
            if (!supportsLocalStorage()) { return false; }
            try {
                localStorage['SQLife__'+key] = value;
            } catch (e) {
                return false;
            }
            return true;
        }
    	function loadCenterPanel(module, title) {
            window.location = 'main.php?m=' + module + '&t=' + title;
    	}
        function dlgOpen(cpt, src) {
            var bwe = window.innerWidth;
            var bhe = window.innerHeight;
            var d = document.getElementById('dlg');
            var id= document.getElementById('idlg');
            var dw = parseInt(0.8*bwe);
            var dh = parseInt(0.8*bhe);
            d.style.display = 'block';
            d.style.position = 'fixed';
            d.style.left = parseInt((bwe-dw)/2)+"px";
            d.style.top = parseInt((bhe-dh)/2)+"px";
            id.style.width = (dw-10)+"px";
            id.style.height = (dh-20)+"px";
            id.src = src;
            //document.getElementById('cdlg').innerHTML = cpt;
            $("#cdlg").html(cpt);
        }
        function dlgClose() { 
            document.getElementById('idlg').src = 'about:blank'; 
            document.getElementById('dlg').style.display = 'none'; 
        }
        function gohome() {
            window.location = "/index.php";
        }
        $(document).ready(function(){
            if (GETTITLE != "") {
                $('#leftMenu').accordion('select',GETTITLE);
            }
            try {
                mod_startup();
            } catch(ex) {
                // do nothing;
            }
            try {
                lef_startup();
            } catch(ex) {
                // do nothing;
            }
        });
    </script>
</head>
<body class="easyui-layout">
    <div class="north" data-options="region:'north',border:false"><h1>SQLife</h1></div>
    <div class="south" data-options="region:'south',border:false">SQLife &copy; 2015 by h3ru</div>
    <div id="center" data-options="region:'center',title:'<?=$title?>',cache:false" style="padding: 8px;">
    <?php
    if ($module != "") {
        include_once "manga.frm.{$module}.php";
    }
    ?>
    </div>
    <?php
    include_once "manga.inc.wait.php";
    ?>
</body>
</html>