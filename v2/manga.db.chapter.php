<?php
require_once("functions.php");

$fn = isset($_GET["f"]) ? $_GET["f"] : "";
$pr = isset($_GET["p"]) ? $_GET["p"] : "";
$mid = isset($_GET["m"]) ? $_GET["m"] : "";

$mga = new PDO("sqlite:list.db3",null,null);
$mga->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$mgs = $mga->prepare("select * from mlist where manga_id=?");
$mgs->execute(array($mid));
$mgr = $mgs->fetch();
$DBSET = "../__data/{$mgr[4]}";
$mgs = null;
$mga = null;

function get() {
    global $DBSET;
    $dbh = new PDO("sqlite:{$DBSET}", null, null);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $page = isset($_POST["page"]) ? $_POST["page"] : 1;
    $rows = isset($_POST["rows"]) ? $_POST["rows"] : 25;
    $offset = ($page-1)*$rows;

    $result = array();
    
    try {
        $qry = "select chapterid,chapter,link,pages,donecount,pagethumbid from chapters";
        $stm = $dbh->prepare($qry);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_CLASS);
        $count = count($data);
        
        $result = array("msg"=>"OK", "rows"=>$data, "total"=>$count);
    } catch (PDOException $e) {
        $result = array("msg"=>"PDO ERROR :: ".$e->getMessage(), "rows"=>array(), "total"=>0);
    } catch (Exception $e) {
        $result = array("msg"=>"ERROR :: ".$e->getMessage(), "rows"=>array(), "total"=>0);
    }

    print json_encode($result);
    $stm = null;
    $dbh = null;
}

function set() {
    global $DBSET;
    $dbh = new PDO("sqlite:{$DBSET}", null, null);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = array();
    $data = array();
    $error = "OK";

    $id = isset($_POST["manga_id"]) ? $_POST["manga_id"] : 0;
    if ($id > 0) {
        $qry = "
            update mlist 
            set title=?, server_key=?, server=?, db_name=?, last_update=CURRENT_TIMESTAMP,
                info=?, tag=?, bintang=?, note=?
            where manga_id={$id}
        ";
        try {
            $stm = $dbh->prepare($qry);
            $param = array(
                $_POST["title"],
                $_POST["server_key"],
                $_POST["server"],
                $_POST["db_name"],
                $_POST["info"],
                $_POST["tag"],
                $_POST["bintang"],
                $_POST["note"]
            );
            $stm->execute($param);
        } catch (PDOException $e) {
            $error = $e->getMessage();
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    } else {
        $qry = "
            insert into mlist (title, server_key, server, db_name, info, tag, 
            bintang, note, last_update) values (?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP)
        ";
        try {
            $stm = $dbh->prepare($qry);
            $param = array(
                $_POST["title"],
                $_POST["server_key"],
                $_POST["server"],
                $_POST["db_name"],
                $_POST["info"],
                $_POST["tag"],
                $_POST["bintang"],
                $_POST["note"]
            );
            $stm->execute($param);
            $id = $dbh->lastInsertId();
            //$dbh->commit();
        } catch (PDOException $e) {
            $error = $e->getMessage();
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
    
    if ($error == "OK") {
        $qry = "select * from mlist where manga_id=?";
        try {
            $stm = $dbh->prepare($qry);
            $stm->execute(array($id));
            $data = $stm->fetchAll(PDO::FETCH_CLASS);
        } catch (PDOException $e) {
            $error = $e->getMessage();
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }

    $result = array(
        "msg"=>$error,
        "rows"=>$data,
        "row_idx"=>$_POST["row_idx"]
    );
    print json_encode($result);
    $stm = null;
    $dbh = null;
}

function getCombo() {
    global $DBSET;
    $db = new medoo($DBSET);

    $recs = $db->select("status",array("status_id","description"),array("ORDER"=>"description"));
    $dbe = $db->error();
    if ($dbe[0]!=0) {
        $result = array("msg"=>join(" | ", $dbe), "total"=>0, "rows"=>array());
    } else {
        //$result = array("msg"=>"OK", "total"=>count($recs), "rows"=>$recs);
        $result = $recs;
    }
    print json_encode($result);
}

function del() {
    global $DBSET;
    $dbh = new PDO("sqlite:{$DBSET}", null, null);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $result = array();
    
    try {
        $qry = "delete from mlist where manga_id=?";
        $stm = $dbh->prepare($qry);
        $stm->execute(array($_POST["manga_id"]));
        
        $result = array("msg"=>"OK", "id"=>$_POST["del_index"]);
    } catch (PDOException $e) {
        $result = array("msg"=>"PDO ERROR :: ".$e->getMessage(), "id"=>$_POST["del_index"]);
    } catch (Exception $e) {
        $result = array("msg"=>"ERROR :: ".$e->getMessage(), "id"=>$_POST["del_index"]);
    }

    print json_encode($result);
    $stm = null;
    $dbh = null;
}

function lookup() {
    global $DBSET;
    $db = new medoo($DBSET);

    $field = isset($_GET["field"]) ? $_GET["field"] : "";
    $filter= isset($_GET["filter"]) ? $_GET["filter"] : "";

    $recs = $db->query("SELECT {$field} FROM status WHERE {$filter}")->fetchAll();
    $dbe = $db->error();
    if ($dbe[0]!=0) {
        $result = array("msg"=>join(" | ", $dbe), "total"=>0, "rows"=>array());
    } else {
        $result = array("msg"=>"OK", "total"=>count($recs), "rows"=>$recs);
    }
    print json_encode($result);
}

function getRef() {
    global $DBSET;
    $db = new medoo($DBSET);

    $recs = $db->select("status",array("status_id","description"),array("ORDER"=>"status_id"));
    $dbe = $db->error();
    if ($dbe[0]!=0) {
        $result = array("msg"=>join(" | ", $dbe), "total"=>0, "rows"=>array());
    } else {
        $result = array("msg"=>"OK", "total"=>count($recs), "rows"=>$recs);
    }
    print json_encode($result);
}

function tree() {
    global $DBSET;
    $db = new medoo($DBSET);
    $qry = "
        WITH RECURSIVE
            under_root(id, parentId, name) AS (
                VALUES(1,0,'ROOT')
                UNION ALL
                SELECT Folders.FolderID, Folders.ParentID, Folders.FolderName FROM Folders 
                Join under_root on Folders.ParentID=under_root.ID    
                order by 2 desc
            )
        SELECT * FROM under_root
    ";
    $data = $db->query($qry)->fetchAll(PDO::FETCH_CLASS);
    print json_encode($data);
}

function info() {
    global $DBSET;
    $db = new medoo($DBSET);
    $count_sizef = filesize64($DBSET);
    
    $qry = "select coalesce(count(folderid),0) ctr  from folders";
    $count_folders = $db->query($qry)->fetchAll(PDO::FETCH_CLASS)[0]->ctr;
    
    $qry = "select coalesce(count(fileid),0) ctr, coalesce(count(distinct itemid),0) ctri, coalesce(sum(size),0) sizes  from files";
    $data = $db->query($qry)->fetchAll(PDO::FETCH_CLASS)[0];
    $count_files = $data->ctr;
    $count_items = $data->ctri;
    $count_size = $data->sizes;
    $data = array(
        "folders" => number_format($count_folders),
        "files" => number_format($count_files),
        "unique_files" => number_format($count_items),
        "bytes_data" => number_format($count_size),
        "file_size" => number_format($count_sizef)
    );
    print json_encode($data);
}

if ($fn == "get") get();
if ($fn == "set") set();
if ($fn == "ren") ren();
if ($fn == "combo") getCombo();
if ($fn == "del") del();
if ($fn == "lookup") lookup();
if ($fn == "ref") getRef();
if ($fn == "bc") breadCrumb();
if ($fn == "tree") tree();
if ($fn == "i") info();
?>