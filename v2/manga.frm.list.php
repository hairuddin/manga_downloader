<?php
$PAGE_NAME = "Manga List";
$EDITOR_TITLE = "Add Manga";
$PAGE_DB = "manga.db.list.php";
$ACT_GET = "{$PAGE_DB}?d={$DB}&f=get";
$ACT_SET = "{$PAGE_DB}?d={$DB}&f=set";
$ACT_DEL = "{$PAGE_DB}?d={$DB}&f=del";
$DLG_WIDTH = "500px;";
$DLG_HEIGHT= "340px;";
?>
<!-- /-------------------------------------------------------\ -->
<!-- | DATA GRID                                             | -->
<!-- \-------------------------------------------------------/ -->
<table id="grdMain"
       class="easyui-datagrid"
       title="<?= $PAGE_NAME ?>"
       style="height: 100%;"
       toolbar="#tb1"
       url="<?= $ACT_GET ?>"
       rownumbers="false"
       fitColumns="false"
       singleSelect="true"
       collapsible="false"
       pagination="false"
       remoteFilter="true"
       pageList="[25,50,75,100]"
       pageSize="25"
       data-options="onRowContextMenu:function(e) { e.preventDefault(); $('#mm').menu('show', { left: e.pageX, top: e.pageY }); }"
>
    <thead>
	<tr>
        <!-- @@ EDIT FIELDS START HERE -->
	    <th field="manga_id" hidden="true">manga id</th>
        <th field="title" width="500">Title</th>
        <th field="server_key" width="150">Svr Key</th>
	    <th field="server"  width="100">Server</th>
	    <th field="db_name" width="100">DB</th>
        <th field="last_update" width="150">LstUpd</th>
        <th field="info" width="150">Info</th>
	    <th field="tag" width="100">Tag</th>
	    <th field="bintang" width="100">Rating</th>
        <th field="note" width="200">Note</th>
        <!-- @@ EDIT FIELDS STOP HERE -->
	</tr>
    </thead>
</table>
<!-- /-------------------------------------------------------\ -->
<!-- | DATA GRID - TOOLBAR                                   | -->
<!-- \-------------------------------------------------------/ -->
<div id="tb1" style="padding: 5px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-play" plain="false" onclick="javascript:openItem()">Open</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="javascript:newItem()">Add</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="javascript:editItem()">Edit</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="false" onclick="javascript:delItem()">Del</a>
    <span class="btn-separator"></span>
</div>
<!-- /-------------------------------------------------------\ -->
<!-- | FORM EDIT                                             | -->
<!-- \-------------------------------------------------------/ -->
<div id="win" class="easyui-window" title="Edit" modal="true" closed="true" iconCls="icon-edit" collapsible="false" minimizable="false" closable="false" maximizable="false" style="width:<?= $DLG_WIDTH ?>height:<?= $DLG_HEIGHT ?>padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'" style="padding:10px;">
            <div class="ftitle">Manga</div>
            <form id="frm1" method="post">
                <!-- @@ START EDIT HERE -->
                <input type="hidden" id="row_idx" name="row_idx" value="">
                <input type="hidden" id="manga_id" name="manga_id" value="">
                <input type="hidden" id="last_update" name="last_update" value="">
                <table cellpadding="5" style="width:100%;">
                    <tr>
                        <td style="width: 30%;">Title:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="title" name="title" required="true" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Server Key:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="server_key" name="server_key" required="true" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Server Name:</td>
                        <td style="width: 70%;"><input class="easyui-combobox" id="server" name="server" required="true" style="width:100%;" data-options="valueField:'id',textField:'text',url:'__servers.php'"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">DB Name:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="db_name" name="db_name" required="true" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Tag:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="tag" name="tag" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Rating:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="bintang" name="bintang" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Note:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="note" name="note" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Info:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="info" name="info" style="width:100%;"></td>
                    </tr>
                </table>
                <!-- @@ EDIT STOP HERE -->
            </form>
        </div>
        <div data-options="region:'south',border:false" style="text-align:right;padding:5px 0 0;">
            <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" style="width: 100px;" onclick="saveItem()">Submit</a> &nbsp;
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" style="width: 100px;" onclick="$('#win').window('close');">Cancel</a>
        </div>
    </div>
</div>
<!-- /-------------------------------------------------------\ -->
<!-- | CONTEXT MENU                                          | -->
<!-- \-------------------------------------------------------/ -->
<div id="mm" class="easyui-menu" style="width:120px;">
    <div data-options="iconCls:'icon-play'" onclick="javascript:openItem()">Open</div>
    <div data-options="iconCls:'icon-edit'" onclick="javascript:editItem()">Rename</div>
</div>
<!-- /-------------------------------------------------------\ -->
<!-- | PAGE SCRIPTING                                        | -->
<!-- \-------------------------------------------------------/ -->
<script type="text/javascript">
    var PID = -1;
    function afterReply(data) {
        $('#wait').window('close');
        if (data.msg != "OK") { 
            $.messager.alert('<?= $PAGE_NAME ?>',data.msg,'error');
        } else {
            //$("#grdMain").datagrid("reload");
            if (data.row_idx != "-1") {
                $("#grdMain").datagrid("updateRow",{index: data.row_idx, row: data.rows[0]});
            } else {
                $("#grdMain").datagrid("appendRow",data.rows[0]);
                //$("#grdMain").datagrid("refreshRow", data.row_idx);
            }
        }
    }
    function afterReplyDel(data) {
        $('#wait').window('close');
        if (data.msg != "OK") { 
            $.messager.alert('<?= $PAGE_NAME ?>',data.msg,'error');
        } else {
            $("#grdMain").datagrid("deleteRow", data.id);
        }
    }
    function editItem() {
        var row = $('#grdMain').datagrid('getSelected');
        if (row) {
            $('#win').window({iconCls:'icon-edit'});
            $('#win').window('open').window('setTitle','&nbsp; Edit'); 
            $('#frm1').form('load',row);
            $('#row_idx').val($('#grdMain').datagrid('getRowIndex', row));
        }
    }
    function newItem() {
        $('#win').window({iconCls:'icon-add'});
        $('#win').window('open').window('setTitle','&nbsp; New Data');
        $('#frm1').form('clear');
        // @@ EDIT FIELD NAME
        $('#manga_id').val("0");
        $('#row_idx').val("-1");
    }
    function openItem() {
        var row = $('#grdMain').datagrid('getSelected');
        $("#wait").window("open");
        if (row) {
            window.location = 'main.php?m=chapter&t=Chapter+List&manga_id=' + row.manga_id;
        }
        $("#wait").window("close");
    }
    function delItem() {
        var row = $('#grdMain').datagrid('getSelected');
        var idx = $('#grdMain').datagrid('getRowIndex', row);
        if (row) {
            $.messager.confirm('<?= $PAGE_NAME ?>','Are you sure to delete this record?', function(r){
                if (r) {
                    $('#wait').window('open');
                    // @@ EDIT FIELD NAME
                    $.ajax({
                        type : "POST",
                        dataType: "json", // sudah json, jadi tanpa eval lagi...
                        data: { "manga_id": row.manga_id, "del_index": idx },
                        url: "<?= $ACT_DEL ?>",
                        success: function(data) { 
                            //var data = eval('('+result+')'); 
                            afterReplyDel(data); 
                        }
                    });
                }
            });
        }
    }
    function saveItem() {
        $('#frm1').form('submit',{
            url: "<?= $ACT_SET ?>",
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(result) { 
                var data = eval('('+result+')'); 
                afterReply(data); 
            }
        });
        $("#win").window("close");
        $("#wait").window("open");
    }
    function format_num(vals, row) {
        if (vals != undefined) {
            return parseInt(vals).format();
        } else {
            return "";
        }
    }
    function mod_startup() {
        //setupBreadCrumb(<?=$ID?>);
        //var $img = $("#iprv").imgViewer();
    }
</script>