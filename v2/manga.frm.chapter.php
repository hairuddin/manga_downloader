<?php
$MID = isset($_GET["manga_id"]) ? $_GET["manga_id"] : 0;
$PAGE_NAME = "Chapter List";
$EDITOR_TITLE = "Add Manga";
$PAGE_DB = "manga.db.chapter.php";
$ACT_GET = "{$PAGE_DB}?m={$MID}&f=get";
$ACT_SET = "{$PAGE_DB}?m={$MID}&f=set";
$ACT_DEL = "{$PAGE_DB}?m={$MID}&f=del";
$DLG_WIDTH = "500px;";
$DLG_HEIGHT= "340px;";
?>
<!-- /-------------------------------------------------------\ -->
<!-- | DATA GRID                                             | -->
<!-- \-------------------------------------------------------/ -->
<table id="grdMain"
       class="easyui-datagrid"
       title="<?= $PAGE_NAME ?>"
       style="height: 100%;"
       toolbar="#tb1"
       url="<?= $ACT_GET ?>"
       rownumbers="false"
       fitColumns="true"
       singleSelect="true"
       collapsible="false"
       pagination="false"
       remoteFilter="true"
       pageList="[25,50,75,100]"
       pageSize="25"
       data-options="onRowContextMenu:function(e) { e.preventDefault(); $('#mm').menu('show', { left: e.pageX, top: e.pageY }); }"
>
    <thead>
	<tr>
        <!-- @@ EDIT FIELDS START HERE -->
	    <th field="CHAPTERID" hidden="true">manga id</th>
        <th field="CHAPTER" width="350">Chapter</th>
        <th field="LINK" width="350">Link</th>
	    <th field="PAGES"  width="50">Pages</th>
	    <th field="DONECOUNT" width="50">Done Pages</th>
        <th field="PAGETHUMBID" hidden="true">thumb</th>
        <!-- @@ EDIT FIELDS STOP HERE -->
	</tr>
    </thead>
</table>
<!-- /-------------------------------------------------------\ -->
<!-- | DATA GRID - TOOLBAR                                   | -->
<!-- \-------------------------------------------------------/ -->
<div id="tb1" style="padding: 5px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-play" plain="false" onclick="javascript:openItem()">Open</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="javascript:newItem()">Add</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="javascript:editItem()">Edit</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="false" onclick="javascript:delItem()">Del</a>
    <span class="btn-separator"></span>
</div>
<!-- /-------------------------------------------------------\ -->
<!-- | FORM EDIT                                             | -->
<!-- \-------------------------------------------------------/ -->
<div id="win" class="easyui-window" title="Edit" modal="true" closed="true" iconCls="icon-edit" collapsible="false" minimizable="false" closable="false" maximizable="false" style="width:<?= $DLG_WIDTH ?>height:<?= $DLG_HEIGHT ?>padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'center'" style="padding:10px;">
            <div class="ftitle">Manga</div>
            <form id="frm1" method="post">
                <!-- @@ START EDIT HERE -->
                <input type="hidden" id="row_idx" name="row_idx" value="">
                <input type="hidden" id="CHAPTERID" name="CHAPTERID" value="">
                <input type="hidden" id="PAGETHUMBID" name="PAGETHUMBID" value="">
                <table cellpadding="5" style="width:100%;">
                    <tr>
                        <td style="width: 30%;">Chapter:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="CHAPTER" name="CHAPTER" required="true" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Link:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="LINK" name="LINK" required="true" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Pages:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="PAGES" name="PAGES" style="width:100%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Done Count:</td>
                        <td style="width: 70%;"><input class="easyui-textbox" type="text" id="DONECOUNT" name="DONECOUNT" style="width:100%;"></td>
                    </tr>
                </table>
                <!-- @@ EDIT STOP HERE -->
            </form>
        </div>
        <div data-options="region:'south',border:false" style="text-align:right;padding:5px 0 0;">
            <a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" style="width: 100px;" onclick="saveItem()">Submit</a> &nbsp;
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" style="width: 100px;" onclick="$('#win').window('close');">Cancel</a>
        </div>
    </div>
</div>
<!-- /-------------------------------------------------------\ -->
<!-- | CONTEXT MENU                                          | -->
<!-- \-------------------------------------------------------/ -->
<div id="mm" class="easyui-menu" style="width:120px;">
    <div data-options="iconCls:'icon-play'" onclick="javascript:openItem()">Open</div>
    <div data-options="iconCls:'icon-edit'" onclick="javascript:editItem()">Rename</div>
</div>
<!-- /-------------------------------------------------------\ -->
<!-- | PAGE SCRIPTING                                        | -->
<!-- \-------------------------------------------------------/ -->
<script type="text/javascript">
    var PID = -1;
    function afterReply(data) {
        $('#wait').window('close');
        if (data.msg != "OK") { 
            $.messager.alert('<?= $PAGE_NAME ?>',data.msg,'error');
        } else {
            //$("#grdMain").datagrid("reload");
            if (data.row_idx != "-1") {
                $("#grdMain").datagrid("updateRow",{index: data.row_idx, row: data.rows[0]});
            } else {
                $("#grdMain").datagrid("appendRow",data.rows[0]);
                //$("#grdMain").datagrid("refreshRow", data.row_idx);
            }
        }
    }
    function afterReplyDel(data) {
        $('#wait').window('close');
        if (data.msg != "OK") { 
            $.messager.alert('<?= $PAGE_NAME ?>',data.msg,'error');
        } else {
            $("#grdMain").datagrid("deleteRow", data.id);
        }
    }
    function editItem() {
        var row = $('#grdMain').datagrid('getSelected');
        if (row) {
            $('#win').window({iconCls:'icon-edit'});
            $('#win').window('open').window('setTitle','&nbsp; Edit'); 
            $('#frm1').form('load',row);
            $('#row_idx').val($('#grdMain').datagrid('getRowIndex', row));
        }
    }
    function newItem() {
        $('#win').window({iconCls:'icon-add'});
        $('#win').window('open').window('setTitle','&nbsp; New Data');
        $('#frm1').form('clear');
        // @@ EDIT FIELD NAME
        $('#CHAPTERID').val("0");
        $('#row_idx').val("-1");
    }
    function openItem() {
        var row = $('#grdMain').datagrid('getSelected');
        $("#wait").window("open");
        if (row) {
            if (row.contenttype == 'folder') window.location = 'main.php?db=<?=$DB?>&id=' + row.fileid;
            if (row.contenttype.substr(0,6)=="image/") {
                $("#iprv").attr("src","");
                $("#prv").window("open");
                $("#iprv").attr("src","file.php?db=<?= $DB ?>&id="+row.fileid);
            }
        }
        $("#wait").window("close");
    }
    function delItem() {
        var row = $('#grdMain').datagrid('getSelected');
        var idx = $('#grdMain').datagrid('getRowIndex', row);
        if (row) {
            $.messager.confirm('<?= $PAGE_NAME ?>','Are you sure to delete this record?', function(r){
                if (r) {
                    $('#wait').window('open');
                    // @@ EDIT FIELD NAME
                    $.ajax({
                        type : "POST",
                        dataType: "json", // sudah json, jadi tanpa eval lagi...
                        data: { "CHAPTERID": row.CHAPTERID, "del_index": idx },
                        url: "<?= $ACT_DEL ?>",
                        success: function(data) { 
                            //var data = eval('('+result+')'); 
                            afterReplyDel(data); 
                        }
                    });
                }
            });
        }
    }
    function saveItem() {
        $('#frm1').form('submit',{
            url: "<?= $ACT_SET ?>",
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(result) { 
                var data = eval('('+result+')'); 
                afterReply(data); 
            }
        });
        $("#win").window("close");
        $("#wait").window("open");
    }
    function format_num(vals, row) {
        if (vals != undefined) {
            return parseInt(vals).format();
        } else {
            return "";
        }
    }
    function mod_startup() {
        //setupBreadCrumb(<?=$ID?>);
        //var $img = $("#iprv").imgViewer();
    }
</script>