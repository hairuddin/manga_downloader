<?php
    header('Access-Control-Allow-Headers: X-Requested-With, origin, content-type');
    header('Access-Control-Allow-Origin: *');

    function currentdir($url) {
        // note: anything without a scheme ("example.com", "example.com:80/", etc.) is a folder
        // remove query (protection against "?url=http://example.com/")
        if ($first_query = strpos($url, '?')) $url = substr($url, 0, $first_query);
        // remove fragment (protection against "#http://example.com/")
        if ($first_fragment = strpos($url, '#')) $url = substr($url, 0, $first_fragment);
        // folder only
        $last_slash = strrpos($url, '/');
        if (!$last_slash) {
            return '/';
        }
        // add ending slash to "http://example.com"
        if (($first_colon = strpos($url, '://')) !== false && $first_colon + 2 == $last_slash) {
            return $url . '/';
        }
        return substr($url, 0, $last_slash + 1);
    }

    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        die("{$fname} not found in filesystem.\nDatabase file not found.");
    }

    $pageid = isset($_POST['page']) ? $_POST['page'] : 0;
    $chapid = isset($_POST['chap']) ? $_POST['chap'] : 0;
    $idxmod = isset($_POST['idx']) ? strtolower($_POST['idx']) : "first";
    
    require("db.php");

    $strcon = array();
    $strcon["DB_DSN"] = "sqlite:".$fname;
    $strcon["DB_USER"] = "";
    $strcon["DB_PASS"] = "";
    $dbh = new DB($strcon);

    $data = null;
    $idx = null;
    if ($pageid>0) {
        $sql = "select pageid, pagenum, chapterid from pages where chapterid=(select chapterid from pages where pageid=?)";
        $data = $dbh->run($sql, array($pageid))->fetchAll();
        $idx = $dbh->run("select pagenum from pages where pageid=?", array($pageid))->fetchColumn();
        $idx = intval($idx)-1;
        try {
            $chapid = $data[$idx]["CHAPTERID"];
        } catch (Exception $e) {
            //
        }
    } elseif ($chapid>0) {
        $sql = "select pageid, pagenum, chapterid from pages where chapterid=?";
        $data = $dbh->run($sql, array($chapid))->fetchAll();
        if ($idxmod=="first") {
            $idx = 0;
        } else {
            $idx = $dbh->run("select max(pagenum) lastpage from pages where chapterid=?", array($chapid))->fetchColumn();
            $idx = intval($idx)-1;
        }
        try {
            $pageid = $data[$idx]["PAGEID"];
        } catch (Exception $e) {
            //
        }
    } else {
        $dat1 = $dbh->run("select pageid, pagenum, chapterid from pages where pageid=(select coalesce(min(fvalue),'1') from manga where fkey='LAST_PAGE')")->fetch();
        $pageid = $dat1["PAGEID"];
        $chapid = $dat1["CHAPTERID"];
        $idx = intval($dat1["PAGENUM"])-1;
        
        $sql = "select pageid, pagenum, chapterid from pages where chapterid=(select chapterid from pages where pageid=?) orderby pagenum";
        $data = $dbh->run($sql, array($pageid))->fetchAll();
    }
    
    $base = currentdir($_SERVER['PHP_SELF'])."file.php?db={$dbname}&id=";
    $urls = array();
    foreach($data as $row){
        $urls[] = array('url' => 'http://'.$_SERVER['HTTP_HOST'].$base.$row["PAGEID"]);
    }
    
    $cname = $dbh->run("select chapter from chapters where chapterid=?",array($chapid))->fetchColumn();
    
    $hasil = array(
        "db" => $dbname,
        "pageid" => $pageid,
        "chapid" => $chapid,
        "indeks" => $idx,
        "pages" => $data,
        "urls" => $urls,
        "caption"=> $cname,
    );
    
    header('Content-Type: application/json');
    echo json_encode($hasil);
?>