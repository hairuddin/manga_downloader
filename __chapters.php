<?php
    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    
    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    require("table.php");
    require("db.php");
    //$dbh = new PDO("sqlite:{$fname}");
    //$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $strcon = array();
    $strcon["DB_DSN"] = "sqlite:".$fname;
    $strcon["DB_USER"] = "";
    $strcon["DB_PASS"] = "";
    $dbh = new DB($strcon);

    //$stm = $dbh->query("select CHAPTERID, CHAPTER, LINK, PAGES, coalesce(DONECOUNT,0) DONE from chapters order by ChapterId");
    //$rst = $stm->fetch(PDO::FETCH_ASSOC);
    $stm = $dbh->run("select CHAPTERID, CHAPTER, LINK, PAGES, coalesce(DONECOUNT,0) DONE from chapters order by ChapterId")->fetchAll();
    
    $t = new PTable("tbl1","data");
    $t->open("thead")
      ->fillTr("th","ID|Chapter|Link|Pages|Done|Action")
      ->close("thead");
    //echo "<table class='data'>\n<tr><th>ID</th><th>Chapter</th><th>Link</th><th>Pages</th><th>Done</th><th>Action</th></tr>\n";
    foreach ($stm as $cep) {
        $id = $cep['CHAPTERID'];
        $namaklas = "";
        //if ($cep['PAGES'] != $cep['DONE']) $namaklas = " class='merah'";
        if ($cep['PAGES'] != $cep['DONE']) $namaklas = "merah";

        $t->fillTr(
            "td",
            array(
                array("td"=>$id, "id"=>"id".$id),
                array("id"=>"ch".$id, "td"=>"<a href='javascript:readChapter({$id},\"".urlencode($cep['CHAPTER'])."\")'>{$cep['CHAPTER']}</a>"),
                array("id"=>"lnk".$id, "td"=>$cep['LINK']),
                array("id"=>"pg".$id, "td"=>$cep['PAGES']),
                array("id"=>"don".$id, "class"=>$namaklas, "td"=>$cep['DONE']),
                "<a href='javascript:skipChapter({$id})'>[skip]</a>
                <a href='javascript:openPages({$id})'>[pages]</a>
                <a href='javascript:downloadChapter({$id},\"{$cep['CHAPTER']}\",0)'>[zip]</a>"
            )
        );
        /*
        echo "<tr>
            <td id='id{$id}'>{$id}</td>
            <td id='ch{$id}'><a href='javascript:readChapter({$id},\"".urlencode($cep['CHAPTER'])."\")'>{$cep['CHAPTER']}</a></td>
            <td id='lnk{$id}'>{$cep['LINK']}</td>
            <td id='pg{$id}'>{$cep['PAGES']}</td>
            <td id='don{$id}'{$namaklas}>{$cep['DONE']}</td>".
            "<td>
                <a href='javascript:skipChapter({$id})'>[skip]</a>
                <a href='javascript:openPages({$id})'>[pages]</a>
                <a href='javascript:downloadChapter({$id},\"{$cep['CHAPTER']}\",0)'>[zip]</a>
            </td></tr>\n";
        */
    }
    //echo "</table>\n";
    echo $t->toString();
?>