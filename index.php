<?php
require_once("cfg.php");
//$dirs = array_filter(glob('__servers/*'), 'is_dir');
$dirs = array();

if ($handle = opendir('__servers')) {
    while (false !== ($entry = readdir($handle))) {
        //echo "$entry\n";
        if ($entry!="." && $entry!="..")
            $dirs[] = $entry;
    }
    closedir($handle);
}

//var_dump($dirs);

$cbx_server = "<select id='servers' name='servers'>";
foreach($dirs as $dir) {
    $svr = strtoupper(str_replace("__servers/","",$dir));
    $cbx_server .= "<option value='{$svr}'>{$svr}</value>";
}
$cbx_server .= "</select>";

$REGTO = (isset($CLP_Company)) ? "<span class='badge2' title='License'><i class='icon-user'></i> Registered to {$CLP_Company}</span>" : "";
?>
<html>
    <head>
        <title>Reader</title>
        <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="default.css">
        <script defer src="fontawesome-free-5.0.13/svg-with-js/js/fontawesome-all.min.js"></script>
        <script type="text/javascript" src="jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="jquery-ui/jquery-ui.min.js"></script>
        <script type="text/javascript" src="jquery.floatThead.min.js"></script>
        <script type="text/javascript" src="numeral.min.js"></script>
        <script type="text/javascript" src="default.js"></script>
        <script type="text/javascript">
            var lstDbs;
            var lstIds = 0;
            var qdb = [];
            var fmt_num = "0,0";
            var LAST_CLASS="garis1";
            var VIEW_ALL = false;

            function dlgOpen(cpt, src) {
                var bwe = window.innerWidth;
                var bhe = window.innerHeight;
                var d = $$('dlg');
                var id= $$('idlg');
                id.src = "about:blank";
                dw = 0.8*bwe;
                dh = 0.8*bhe;
                d.style.position = 'fixed';
                d.style.left = (bwe-dw)/2;
                d.style.top = (bhe-dh)/2;
                id.style.width = dw-10;
                id.style.height = dh-20;
                d.style.display = 'block';
                $$('cdlg').innerHTML = cpt;
                id.src = src;
            }

            function dlgClose() {
                $$('idlg').src = 'about:blank';
                $$('dlg').style.display = 'none';
            }

            function buka_info(db)
            {
                dlgOpen(' Info','tbl_browser.php?db='+db);
            }

            function startLoadInfo() {
                setTimeout(loadInfo, 50);
            }

            function refreshInfo(x) {
                qdb.push(x);
                startLoadInfo();
            }

            function allinfo()
            {
                var j;
                for (j=0; j<lstDbs.length; j++) qdb.push(j);
                startLoadInfo();
            }

            function tagclass(vtag){
                var hasil = "";
                if (vtag.indexOf("DUPLICATE")>=0) hasil += "<span class='blokc'><span title='DUPLICATE' class='fas fa-clone'></span></span>";
                if (vtag.indexOf("NO-IMAGE")>=0) hasil += "<span class='blokc'><span title='NO-IMAGE' class='fas fa-eye-slash'></span></span>";
                if (vtag.indexOf("GORE")>=0) hasil += "<span class='blokc'><span title='GORE' class='fas fa-skull'></span></span>";
                if (vtag.indexOf("KEREN")>=0) hasil += "<span class='blokc'><span title='KEREN' class='fas fa-thumbs-up'></span></span>";
                if (vtag.indexOf("ERROR")>=0) hasil += "<span class='blokc'><span title='ERROR' class='fas fa-bug'></span></span>"; //return "";
                if (vtag.indexOf("NCI")>=0) hasil += "<span class='blokc'><span title='NCI' class='fab fa-hotjar'></span></span>"; //return "";
                if (vtag.indexOf("ESI")>=0) hasil += "<span class='blokc'><span title='ESI' class='fas fa-burn'></span></span>"; //return "icon-fire";
                if (vtag.indexOf("JELEK")>=0) hasil += "<span class='blokc'><span title='JELEK' class='fas fa-thumbs-down'></span></span>"; //return "icon-thumbs-down";
                
                if (hasil=="")  hasil += "<span class='blokc'><span title='"+vtag+"' class='fas fa-play-circle'></span></span>"; //"icon-";
                
                return hasil;
            }

            function loadInfo() {
                if (qdb.length>0)
                {
                    lstIds = qdb.shift();
                    $.ajax({
                        url: '__infodb.php',
                        data: {'db': lstDbs[lstIds].nama},
                        type: "POST", cache: false,
                        success: function(responseText, responseStatus, responseXML) {
                            var minfo;
                            eval("minfo="+responseText+";");
                            $("#svr__"+lstIds).html('@ '+minfo[0]);
                            $("#siz__"+lstIds).html(minfo[1]);
                            $("#chp__"+lstIds).html(numeral(minfo[2]).format(fmt_num));
                            $("#pgs__"+lstIds).html(numeral(minfo[3]).format(fmt_num));
                            lstIds = 0;
                            startLoadInfo();
                        }
                    });
                }
            }

            function doCreate() {
                $.ajax({
                    url: '__createdb.php',
                    data: {
                        'new': $("#new").val(),
                        'loc': $("#loc").val(),
                        'server': $("#servers").val()
                    },
                    type: "POST", cache: false,
                    success: function(responseText, responseStatus, responseXML) {
                        $("#debug").html(responseText);
                        reStart();
                        $("#loc").focus();
                    }
                });
                $("#new").val("");
                $("#loc").val("");
            }

            function reStart() {
                setTimeout(startup, 50);
            }
            
            function startup() {
                //$("#dbs").html("loading data...");
                $.ajax({
                    url: '__listdb.php',
                    data: {},
                    type: "POST", cache: false,
                    success: function(responseText, responseStatus, responseXML) {
                        //eval("lstDbs="+responseText+";");
                        lstDbs = responseText;
                        listBooks();
                    }
                });
            }
            
            function is_hidden(v1, v2){
                if (VIEW_ALL) return false;
                if ((v1.toUpperCase().indexOf("-HIDE-")>=0) && !VIEW_ALL) return true;
                if ((v2.toUpperCase().indexOf("NCI")>=0) && !VIEW_ALL) return true;
                return false;
            }
            
            function openSesame(vtf){
                VIEW_ALL = vtf;
                listBooks();
            }
            
            function listBooks(){
                var ctr = -1;
                var html = "<table id='tbld' class='data2'>";
                html += "<thead><tr><th>No</th><th>Title</th><th>Size</th><th>Chapters</th><th>Pages</th></tr></thead>";
                for (var x=0; x<lstDbs.length; x++) {
                    var infoicon = "fa-info-circle";
                    if (is_hidden(lstDbs[x].categories, lstDbs[x].tag)) {
                        continue;
                    } else {
                        if (lstDbs[x].categories.toUpperCase().indexOf("-HIDE-")>=0) infoicon = "fa-exclamation-triangle";
                    }
                    
                    ctr++;
                    var l0 = ""; 
                    if (lstDbs[x].last_chapter!="") l0 = "<span class='blokc lac'>"+lstDbs[x].last_chapter+" &mdash; "+lstDbs[x].last_page+"</span>";
                    var l1 = "<a href='javascript:buka_info(\""+lstDbs[x].nama+"\")'><span class='fas "+infoicon+"'></span></a>";
                    var la = (lstDbs[x].status.toUpperCase()=="COMPLETED") ? "<span title='COMPLETED' class='blokc'><span class='fas fa-lock'></span></span>" : "<span class='blokc'><span title='ONGOING' class='fas fa-lock-open'></span></span>";
                    var l2 = (lstDbs[x].status!="") ? la : "";
                    var l3 = (lstDbs[x].tag!="") ? ""+tagclass(lstDbs[x].tag.toUpperCase())+"" : "";
                    var lsvr = "<span class='blokc lab' id='svr__"+x+"'>"+lstDbs[x].server.toUpperCase()+"</span>";
                    html += "<tr>";
                    html += "<td class='kanan'><a href='javascript:refreshInfo("+ctr+")'>"+(1+ctr)+".</a></td>";
                    html += "<td>"+l1+" <a href='manga.php?db="+encodeURIComponent(lstDbs[x].nama)+"' title='"+lstDbs[x].nama+"'>"+lstDbs[x].manga+"</a>"+lsvr+l2+l3+l0+"</td>";
                    html += "<td class='kanan'><span id='siz__"+x+"'>"+lstDbs[x].size+"</span></td>";
                    html += "<td class='kanan'><span id='chp__"+x+"'>"+numeral(lstDbs[x].chapters).format(fmt_num)+"</span></td>";
                    html += "<td class='kanan'><span id='pgs__"+x+"'>"+numeral(lstDbs[x].pages).format(fmt_num)+"</span></td>";
                    html += "</tr>";
                }
                html += "</table>";
                $("#dbs").html(html);
                $('#tbld').floatThead({
                    top: $("#atasan").innerHeight(),
                    zIndex: 45
                });
            }

            $(document).ready(function(){
                $("#dbs").html("loading data...");

                document.body.style.paddingTop = $("#atasan").innerHeight();
                startup();
                $(document).tooltip();
            });

            function getKey(event) { /*alert(event.which);*/ }
        </script>
    </head>
    <body>
        <div id="atasan" class="garis1" style="background-color:white; padding: 4px 6px; position: fixed; left:0px; top:0px; right:0px;">
            <span class="badge" title="Main Menu"><a href="index.php" title="HOME"><i class="fas fa-lg fa-home"></i></a><a href="phpinfo.php" title="PHP Info"><i class="fas fa-lg fa-info-circle"></i></a></span>
            <span class="badge2" title="Server"><a href="javascript:openSesame(!VIEW_ALL);"><i class="fas fa-lg fa-globe"></i></a>  <?php echo $_SERVER["HTTP_HOST"]; ?></span>
            <span class="badge2" title="Proxy"><i class="fas fa-lg fa-external-link-alt"></i>  <?php echo ($HTTP_PROXY=="") ? "No Proxy" : $HTTP_PROXY; ?></span>
            <?php echo $REGTO; ?>
            <div><br><big><big><big>Web Reader</big></big></big></div>
            <div>Create new database [location] <input type="text" id="loc" name="loc" value="" onkeyup="getKey(event);"> [file] <input type="text" id="new" name="new" value=""> <?=$cbx_server?> <button class="tbl" onclick="doCreate()"><span class="icon-save"></span> Create</button>
                <button onclick="allinfo()" class="tbl"><span class="icon-info-sign"></span> All Info</button></div>
        </div>
        <div>
            <br>Available data:
            <div id="dbs"><big><i class="icon-spin icon-spinner"></i> Loading data...</big></div>
        </div>
        <div id="dlg" style="display:none;">
            <div class="caption"><a href="javascript:dlgClose()" class="cbtn"><span class="fas fa-lg fa-times-circle"></span></a> <span id="cdlg">Dialog Box Caption</span></div>
            <iframe id="idlg" src="about:blank" style="border-style: none;"></iframe>
        </div>
        <div id="debug"></div>
    </body>
</html>
