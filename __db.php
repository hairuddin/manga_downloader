<?php
    require("db.php");

    $cmd = isset($_POST['cmd']) ? strtolower($_POST['cmd']) : "";

    $hasil = array();
    $hasil["command"]=$cmd;
    $hasil["time"]=date("Y-m-d H:i:s");

    $dbname = isset($_POST['db']) ? $_POST['db'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";

    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        //die("Database file not found.\n{$fname} not found in filesystem.");
        $hasil["error"] = 404;
        $hasil["message"] = "Manga file not found >>".$dbname." >>".$fname;
        header('Content-Type: application/json');
        echo json_encode($hasil);
        die();
    }

    if ($cmd == "hapus"){
        unlink($fname);
        $hasil["error"] = 0;
        $hasil["message"] = "Deleted.";
        header('Content-Type: application/json');
        echo json_encode($hasil);
        die();
    }
    
    //$dbh = new PDO("sqlite:{$fname}");
    //$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $strcon = array();
    $strcon["DB_DSN"] = "sqlite:".$fname;
    $strcon["DB_USER"] = "";
    $strcon["DB_PASS"] = "";
    $dbh = new DB($strcon);

    switch ($cmd) {
        case "reset one page":
            $pid = isset($_POST['pid']) ? $_POST['pid'] : 0;
            if ($pid>0) {
                try {
                    //$dbh->query("update pages set done=0, img=null where done=1 and pageid={$pid}");
                    $dbh->run("update pages set done=0, img=null where done=1 and pageid=?", array($pid));
                    //echo "Success.";
                    $hasil["error"]=0;
                    $hasil["message"]="Success.";
                } catch (Exception $e) {
                    //echo "Error. ".$e;
                    $hasil["error"] = $e->getCode();
                    $hasil["message"] = $e->getMessage();
                    $hasil["trace"] = $e->getTraceAsString();
                }
            }
            break;
        case "open table":
            $tbl = isset($_POST['tbl']) ? $_POST['tbl'] : "";
            if ($tbl!="")
            {
                try {
                    //$has = $dbh->query("select * from {$tbl}")->fetchAll(PDO::FETCH_ASSOC);
                    $has = $dbh->run("select * from ".$tbl)->fetchAll();
                    //$meta = $dbh->query("PRAGMA table_info({$tbl});")->fetchAll(PDO::FETCH_ASSOC);
                    $meta = $dbh->run("PRAGMA table_info({$tbl})")->fetchAll();
                    //echo "{\"error\":\"0\", \"count\":\"".count($has)."\", \"rows\": ".json_encode($has).", \"meta\": ".json_encode($meta)."}";
                    $hasil["error"]=0;
                    $hasil["count"]=count($has);
                    $hasil["rows"]=$has;
                    $hasil["meta"]=$meta;
                    $hasil["message"]="Success.";
                } catch (Exception $e) {
                    $hasil["error"] = $e->getCode();
                    $hasil["message"] = $e->getMessage();
                    $hasil["trace"] = $e->getTraceAsString();
                }
            }
            break;
        case "update var":
            $tbl = isset($_POST['tbl']) ? $_POST['tbl'] : "";
            $nval = isset($_POST['nval']) ? $_POST['nval'] : "";
            $key = isset($_POST['key']) ? $_POST['key'] : "";
            $row = isset($_POST['row']) ? $_POST['row'] : "";
            try {
                //$ctr = current($dbh->query("select count(FKEY) from MANGA where FKEY='{$key}'")->fetch());
                $ctr = $dbh->run("select count(FKEY) from MANGA where FKEY=?", array($key))->fetchColumn();
                if ($ctr>0) {
                    //$stm = $dbh->prepare("update MANGA set FVALUE=? where FKEY=?");
                    //$stm->execute(array($nval, $key));
                    $dbh->run("update MANGA set FVALUE=? where FKEY=?",array($nval, $key));
                } else {
                    //$stm = $dbh->prepare("insert into MANGA (FKEY, FVALUE) values (?,?)");
                    //$stm->execute(array(strtoupper($key), $nval));
                    $dbh->run("insert into MANGA (FKEY, FVALUE) values (?,?)", array(strtoupper($key), $nval));
                }
                //echo "{\"error\":\"0\", \"nval\":\"{$nval}\", \"row\":\"{$row}\"}";
                $hasil["error"]=0;
                $hasil["nval"]=$nval;
                $hasil["row"]=$row;
                $hasil["message"]="Success.";
            } catch (Exception $e) {
                $hasil["error"] = $e->getCode();
                $hasil["message"] = $e->getMessage();
                $hasil["trace"] = $e->getTraceAsString();
            }
            break;
        case "new var":
            $tbl = isset($_POST['tbl']) ? $_POST['tbl'] : "";
            $nval = isset($_POST['nval']) ? $_POST['nval'] : "";
            $nkey = isset($_POST['nkey']) ? $_POST['nkey'] : "";
            try {
                //$stm = $dbh->prepare("insert into MANGA (FKEY, FVALUE) values (?,?)");
                //$stm->execute(array(strtoupper($nkey), $nval));
                $dbh->run("insert into MANGA (FKEY, FVALUE) values (?,?)",array(strtoupper($nkey), $nval));

                //echo "{\"error\":\"0\", \"nkey\":\"{$nkey}\"}";
                $hasil["error"]=0;
                $hasil["nkey"]=$nkey;
                $hasil["message"]="Success.";
            } catch (Exception $e) {
                $hasil["error"] = $e->getCode();
                $hasil["message"] = $e->getMessage();
                $hasil["trace"] = $e->getTraceAsString();
            }
            break;
        case "del var":
            $key = isset($_POST['key']) ? $_POST['key'] : "";
            try {
                //$stm = $dbh->prepare("delete from MANGA where FKEY = ?");
                //$stm->execute(array($key));
                $dbh->run("delete from MANGA where FKEY = ?",array($key));
                //echo "{\"error\":\"0\"}";
                $hasil["error"]=0;
                $hasil["message"]="Success.";
            } catch (Exception $e) {
                $hasil["error"] = $e->getCode();
                $hasil["message"] = $e->getMessage();
                $hasil["trace"] = $e->getTraceAsString();
            }
            break;
        case "vacuum":
            try {
                //$dbh->query("VACUUM");
                $dbh->query("VACUUM");
                //echo "{\"error\":\"0\"}";
                $hasil["error"]=0;
                $hasil["message"]="Success.";
            } catch (Exception $e) {
                $hasil["error"] = $e->getCode();
                $hasil["message"] = $e->getMessage();
                $hasil["trace"] = $e->getTraceAsString();
            }
            break;
        case "append page":
            $num = 0;
            try{
                $ch = isset($_POST['cid']) ? $_POST['cid'] : "";
                if ($ch != ""){
                    $sql = "select coalesce(max(0+pagenum),0) from pages where chapterid=?";
                    $num = $dbh->run($sql,array($ch))->fetchColumn();
                    $dbh->run("insert into pages (pagenum, chapterid, link, imglink, done, imgsize) values (?,?,?,?,?,?)", array(1+$num, $ch, "Manual Append", "Manual Append", 0, 0));
                    $hasil["error"]=0;
                    $hasil["message"]="Success.";
                } else {
                    $hasil["error"] = 50;
                    $hasil["message"] = "CHAPTER_ID not supplied.";
                    $hasil["trace"] = "Setelah di-trace ternyata programmernya bego...";
                }
            } catch(Exception $e){
                $hasil["error"] = $e->getCode();
                $hasil["message"] = $e->getMessage()." [".$num."]";
                $hasil["trace"] = $e->getTraceAsString();
            }
            break;
        case "append chapter":
            $num = 0;
            try{
                $nam = isset($_POST['cname']) ? $_POST['cname'] : "";
                //$sql = "select coalesce(max(0+chapterid),0) from chapters";
                //$num = $dbh->run($sql)->fetchColumn();
                $dbh->run("insert into chapters (chapter, link, pages, donecount) values (?,?,?,?)", array($nam, "", 0, 0));
                $hasil["error"]=0;
                $hasil["message"]="Success.";
            } catch(Exception $e){
                $hasil["error"] = $e->getCode();
                $hasil["message"] = $e->getMessage()." [".$num."]";
                $hasil["trace"] = $e->getTraceAsString();
            }
            break;
    }
    header('Content-Type: application/json');
    echo json_encode($hasil);
?>
