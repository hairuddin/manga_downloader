<?php
    $dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";
    $curpage = isset($_GET['id']) ? $_GET['id'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";
    $fname = "{$dbdir}{$separator}__data{$separator}{$dbname}.mga";
    if (!file_exists("{$fname}")) {
        die("Database file not found.\n{$fname} not found in filesystem.");
    }
    
    try {
	    require("db.php");
	    $strcon = array();
	    $strcon["DB_DSN"] = "sqlite:".$fname;
	    $strcon["DB_USER"] = "";
	    $strcon["DB_PASS"] = "";
	    $dbh = new DB($strcon);

		$tmp_file_name = $_FILES['Filedata']['tmp_name'];
		$namafile = $_FILES['Filedata']['name'];
		$imgf = file_get_contents($tmp_file_name);

	    $qry = "update pages set img=?, imglink=?, done=1 where pageid=?"; //.$curpage;
	    $stt = $dbh->prepare($qry);
	    $stt->bindParam(1, $imgf);
	    $stt->bindParam(2, $namafile);
	    $stt->bindParam(3, $curpage);
	    $stt->execute();

	    $dbh->run("update pages set IMGSIZE=length(hex(img))/2 where pageid=?", array($curpage));
    } catch (Exception $e) {
    	die($e->getMessage());
    }

    echo "OK";
?>