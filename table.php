<?php
class HTable {
    public $Table = "";
    public $Id = "";
    public $ClassName = "";
    function __construct($id, $clsName = ""){
        $this->Id = $id;
        $this->ClassName = $clsName;
        $this->burn();
    }
    public function burn() {
        $this->Table = "<table id='".$this->Id."' class='".$this->ClassName."'>";
        return $this;
    }
    public function toString() {
        return $this->Table."</table>";
    }
    public function open($tag) {
        $this->Table .= "<{$tag}>";
        return $this;
    }
    public function close($tag) {
        $this->Table .= "</{$tag}>";
        return $this;
    }
    public function td($isi) {
        $this->Table .= "<td>".$isi."</td>";
        return $this;
    }
}
class VTable extends HTable {
    public $Startup = "";
    public function info($teks) {
        $this->Table .= "<tr><td colspan='2' class='info'>".$teks."</td></tr>";
        return $this;
    }
    public function edit($field, $caption, $value, $ro=false, $info="") {
        $sro = "";
        if ($ro) $sro = " readonly";
        $sin = "";
        if ($info!="") $sin = " <span class='i'>{$info}</span>";
        $this->Table .= "<tr>".
            "<td><label for='fld_{$field}'>{$caption}{$sin}</label></td>".
            "<td><input type='text' name='{$field}' id='fld_{$field}' value='".$value."'{$sro}></td>".
            "</tr>\n";
        return $this;
    }
    public function editdate($field, $caption, $value, $ro=false, $info="") {
        $sro = "";
        if ($ro) $sro = " readonly";
        $sin = "";
        if ($info!="") $sin = " <span class='i'>{$info}</span>";
        $tvalue = $value;
        if ($tvalue != "") $tvalue = fmt_tgl($tvalue);
        $this->Table .= "<tr>".
            "<td><label for='fldd_{$field}'>{$caption}{$sin}</label></td>".
            "<td><input type='text' name='d_{$field}' id='fldd_{$field}' value='{$tvalue}'{$sro}><input type='hidden' name='{$field}' id='fld_{$field}' value='{$value}'></td>".
            "</tr>\n";
        $this->Startup .= "var js_{$field} = new Pikaday({field: document.getElementById('fldd_{$field}'), firstDay: 1, format: 'DD-MMM-YYYY', onSelect: function(){ $('#fld_{$field}').val(this.getMoment().format('YYYY-MM-DD'));} });\n";
        return $this;
    }
    public function checkbox($field, $caption, $value, $ro=false, $info="") {
        $sro = "";
        if ($ro) $sro = " readonly";
        $sin = "";
        if ($info!="") $sin = " <span class='i'>{$info}</span>";
        $this->Table .= "<tr>".
            "<td><label for='fld_{$field}'>{$caption}{$sin}</label></td>".
            "<td><input type='checkbox' name='{$field}' id='fld_{$field}' value='{$value}'{$sro}></td>".
            "</tr>\n";
        return $this;
    }
    public function combo($field,$caption,$value,$pak,$ro=false,$info=""){
        $sro = "";
        if ($ro) $sro = " readonly";

        $fld_key = $pak[0];
        $fld_text = $pak[1];
        $data = $pak[2];
        $cbx = "<select name='{$field}' id='fld_{$field}'{$ro}>";
        $cbx .= "<option value='0'></option>";
        foreach ($data as $dat) {
            $sel = "";
            if ($value==$dat[$fld_key]) $sel = " selected";
            $cbx .= "<option value='".$dat[$fld_key]."'{$sel}>".$dat[$fld_text]."</option>";
        }
        $cbx .= "</select>";

        $sin = "";
        if ($info!="") $sin = " <span class='i'>{$info}</span>";
        $this->Table .= "<tr>".
            "<td><label for='fld_{$field}'>{$caption}{$sin}</label></td>".
            "<td>{$cbx}</td>".
            "</tr>\n";
        return $this;
    }
    public function getStartup(){return $this->Startup;}
}
class PTable extends HTable {
    public function fillTr($childTag,$isi) {
        $this->open('tr');
        if (is_array($isi)) {
            // isinya array
            foreach ($isi as $item) $this->fillCell($childTag, $item);
        } else {
            // Isinya string delimited with pipe
            $arr = explode("|",$isi);
            foreach ($arr as $i) $this->fillCell($childTag,$i);
        }
        $this->close('tr');
        $this->Table .= "\n";
        return $this;
    }
    public function fillCell($tag,$isi) {
        $hasil = "<".$tag;
        if (is_array($isi)){
            $elm = "";
            foreach($isi as $key=>$value) {
                if ($key != $tag) {
                    if (strlen($elm)>0) $elm .= " ";
                    $elm .= $key."=\"".$value."\"";
                }
            }
            $hasil .= " ".$elm.">".$isi[$tag]."</".$tag.">";
        } else {
            $hasil .= ">".$isi."</".$tag.">";
        }
        $this->Table .= $hasil;
        return $this;
    }
    public static function tdClass($cls, $isi) {
        return array("class"=>$cls, "td"=>$isi);
    }
}

?>