<?php
    ini_set('max_execution_time', 0);
    $err = error_reporting();
    error_reporting(0);

    $dbname = isset($_GET['db']) ? $_GET['db'] : "^_^";
    $chapter = isset($_GET['ch']) ? $_GET['ch'] : "^_^";
    $schap = isset($_GET['str']) ? $_GET['str'] : "^_^";
    $dbdir = dirname(__file__);
    $separator = (substr($dbdir, 0, 1)=='/') ? '/' : "\\";

    $resize = isset($_GET['resize']) ? $_GET['resize'] : "0";
    if ($resize == "") $resize = "0";

    if (!file_exists("{$dbdir}{$separator}__data{$separator}{$dbname}.mga")) {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
    }

    require_once "ZipStream.php";
    require_once "cfg.php";
    require_once "functions.php";

    $dbh = new PDO("sqlite:{$dbdir}{$separator}__data{$separator}{$dbname}.mga");
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $zip = new ZipStream("{$dbname}_{$schap}.cbz");

    $qry = "select * from pages where chapterid={$chapter} order by pagenum";
    $rows = $dbh->query($qry);
    foreach ($rows as $row) {
        $arr = explode(".", $row['IMGLINK']);
        //$isi = $row['IMG'];
        $page = str_pad($row['PAGENUM'],5,"0",STR_PAD_LEFT);
        $nama = $page.".".$arr[count($arr)-1];
        if ($resize != "0") {
            $isi = resizeImageString($row['IMG'], $resize, 0, 80);
            $nama = $page.".jpg";
        } else {
            $isi = $row['IMG'];
        }
        $zip->addFile($isi,$nama);
    }
    $zip->finalize();
?>
